# PROTECTION OF YOUR PERSONAL DATA

This privacy statement provides information about the processing and
the protection of your personal data.

Processing operation: code.europa.eu

Data Controller: European Commission, Directorate-General for Digital
Services (DG DIGIT), Unit B.2 Interoperability and Digital Government
(hereinafter referred to as "DG DIGIT" or "data controller")

Record reference: DPR-EC-17414

- [Introduction](#introduction)
- [Why and how do we process your personal data?](#why-and-how-do-we-process-your-personal-data)
- [On what legal ground(s) do we process your personal data](#on-what-legal-grounds-do-we-process-your-personal-data)
- [Which personal data do we collect and further process?](#which-personal-data-do-we-collect-and-further-process)
- [How long do we keep your personal data?](#how-long-do-we-keep-your-personal-data)
- [How do we protect and safeguard your personal data?](#how-do-we-protect-and-safeguard-your-personal-data)
- [Who has access to your personal data and to whom is it disclosed?](#who-has-access-to-your-personal-data-and-to-whom-is-it-disclosed)
- [What are your rights and how can you exercise them?](#what-are-your-rights-and-how-can-you-exercise-them)
- [Contact information](#contact-information)
- [Where to find more detailed information?](#where-to-find-more-detailed-information)

## Introduction

The European Commission (hereafter 'the Commission') is committed to
protect your personal data and to respect your privacy. The Commission
collects and further processes personal data pursuant to [Regulation
(EU)
2018/1725](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2018.295.01.0039.01.ENG&toc=OJ:L:2018:295:TOC)
of the European Parliament and of the Council of 23 October 2018 on
the protection of natural persons with regard to the processing of
personal data by the Union institutions, bodies, offices and agencies
and on the free movement of such data (repealing Regulation (EC) No
45/2001).

This privacy statement explains the reason for the processing of your
personal data, the way we collect, handle and ensure protection of all
personal data provided, how that information is used and what rights you
have in relation to your personal data. It also specifies the contact
details of the responsible Data Controller with whom you may exercise
your rights, the Data Protection Officer and the European Data
Protection Supervisor.

Code.europa.eu is a free service offered by DG DIGIT, Unit B.2 to the
Commission and/or European Union Institutions, bodies, offices and
agencies (EUIs) and their code-contributors. This service facilitates
public access to and reuse of source code of software and related
information such as build and install scripts, bill of materials,
copyright, attribution, and licence documentation, for which the
Commission and - at their request - other EUIs hold the intellectual
property rights.

The information in relation to the processing operation "code.europa.eu"
undertaken by DIGIT, Unit B.2 of the European Commission is presented
below.

## Why and how do we process your personal data?

**All users, including non-registered users**

The purpose of the code.europa.eu platform is to allow open
collaboration on the development of free and open source software
solutions shared by European Union institutions (EUIs), or in other
words, for which European Union institutions, bodies, offices and
agencies hold the intellectual property rights. It offers several
features to users to find and collaborate on projects and to share their
experience. Therefore, information is made publicly available.

Code.europa.eu can be visited by any user and does not require
registration.

Your personal data will not be used for an automated decision-making
including profiling.

For all visitors, including non-registered users, code.europa.eu is
collecting IP addresses for technical purposes, which are essential for
the operation of the services. An additional processing targets requests
introduced by users via email or fediverse messages. The purpose of this
processing is to answer users' questions, account validation and to
solve any technical issue on the platform.

**Registered users**

Registration and login are used to allow users to voluntary contribute
to the projects on code.europa.eu.

On code.europa.eu, groups of projects can only be created by teams
working for EUIs. On request, project owners working for the Commission
and EUIs, get permission from the controller to create groups.

Inside a group, the project owner(s) decide on the role of contributors,
which can be either Guest, Reporter, Developer, Maintainer, or Owner.
The permissions of each role are explained
[here](https://docs.gitlab.com/ee/user/permissions.html).

Registered users can set-up, edit and change their profile by
voluntarily providing more personal information about them. Such users
can set up their own notifications and preferences, and post in
projects. Registered users can also choose to make their profile visible
to the administrators, only. However, this setting does not hide all
public resources.

Please note that Institutions other than the Commission are responsible
for their subspace and its compliance with the Regulation (EU) 2018/1725
as separate controllers. They must indicate contact options of the
controller and DPO and may provide a data protection notice on their own
governing their subspace if their processing differs from the
Commission's processing.

## On what legal ground(s) do we process your personal data?

The creation, management and implementation of code.europa.eu follows
the Commission Decision of 8 December 2021 on the open source licensing
and reuse of Commission software [(C(2021) 8759 final)](https://ec.europa.eu/transparency/documents-register/detail?ref=C(2021)8759&lang=en).

Therefore, we process your personal data because processing is necessary
for the performance of a task carried out in the public interest or in
the exercise of official authority vested in the Union institution or
body according to Article 5(a) of Regulation (EU) 2018/1725.

For the purpose of processing personal data to register yourself to
code.europa.eu, to edit your profile and to set-up preferences and
notification as well as to respond to request for support the
controller's processing operation is based on consent under Article 5(d)
of Regulation (EU) 2018/1725.

## Which personal data do we collect and further process?

In order to carry out this processing operation the data controller
collects the following categories of personal data:

-   Identification of registered users: email address and username,
    first and family name;
-   Voluntary information submitted by registered users such as photos,
    preferences, job title, bio and social media accounts;
-   Contributor data: The system also processes and makes public the
    time and date of contributions, access rights to projects, and the
    date when a project was joined.
-   Technical data: such as IP address, date and time of authentication.
    This information is not made public

## How long do we keep your personal data?

The data controller only keeps your personal data a as long as you
remain an active member of code.europa.eu. Once your profile remains
inactive for three years, your data will be deleted automatically.

You also have the possibility at any moment to request your account to
be deleted. As explained
[here](https://docs.gitlab.com/ee/user/profile/account/delete_account.html#delete-users-and-user-contributions),
the code.europa.eu administrator can delete user accounts and associated
records. However, this will not remove all associated records. Commits
made by a deleted user will continue to display the username, and some
other records will be moved to system-wide 'Ghost User' account, until
automatic deletion is carried out.

Based on copyright law, all public contributions to the platform will
remain public.

## How do we protect and safeguard your personal data?

All personal data in electronic format (e-mails, documents, databases,
uploaded batches of data, etc.) are stored either on the servers of the
European Commission or of its contractors. All processing operations are
carried out pursuant to the [Commission Decision (EU, Euratom)
2017/46](https://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1548093747090&uri=CELEX:32017D0046)
of 10 January 2017 on the security of communication and information
systems in the European Commission.

The Commission's contractors are bound by a specific contractual
clause for any processing operations of your data on behalf of the
Commission, and by the confidentiality obligations deriving from the
transposition of the General Data Protection Regulation in the EU
Member States ('GDPR' [Regulation (EU)
2016/679](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A32016R0679).

In order to protect your personal data, the Commission has put in place
a number of technical and organisational measures in place. Technical
measures include appropriate actions to address online security, risk of
data loss, alteration of data or unauthorised access, taking into
consideration the risk presented by the processing and the nature of the
personal data being processed. Organisational measures include
restricting access to the personal data solely to authorised persons
with a legitimate need to know for the purposes of this processing
operation.

## Who has access to your personal data and to whom is it disclosed?

All information you voluntary and publicly contribute to code.europa.eu
can be publicly accessed and are made explicitly available to other
users.

Access to your personal data is provided to the Commission staff
responsible for carrying out this processing operation and to authorised
staff according to the "need to know" principle. Such staff abide by
statutory, and when required, additional confidentiality agreements.

Access to your personal data is provided to GitLabHost B.V. located in
Europe and responsible for carrying out processing operations concerning
hosting and support cases on behalf of the controller. The information
we collect will not be given to any third party, except to the extent
and for the purpose we may be required to do so by law.

## What are your rights and how can you exercise them?

You have specific rights as a 'data subject' under Chapter III (Articles
14-25) of Regulation (EU) 2018/1725, in particular the right to access,
your personal data and to rectify them in case your personal data are
inaccurate or incomplete. Where applicable, you have the right to erase
your personal data, to restrict the processing of your personal data, to
object to the processing, and the right to data portability.

You have the right to object to the processing of your personal data,
which is lawfully carried out pursuant to Article 5(1)(a) on grounds
relating to your particular situation. Where applicable, you have the
right to erase your personal data, to restrict the processing of your
personal data, to object to the processing, and the right to data
portability.

In particular, Contributors shall be aware that limited personal data
processed by the controller (name, email address, date and time of their
contribution as well as the time when they joined the project) is made
publicly available. The publication of this personal data is necessary
to comply with Intellectual property law, to build trust within the
developers' community and it is essential to the functioning of the
platform, as it allows for open collaboration and development of
open-source project.

The controller will, therefore, take the above into account when
assessing requests from data subjects concerning certain of their
rights. As an example, an erasure request from a contributor would be
balanced against the legitimate public interest of the controller to
carry out such processing to ensure development of open source software
as well as cybersecurity concerns.

You have consented to provide your personal data to DG DIGIT Unit B.2
for the present processing operation. You can withdraw your consent at
any time by notifying the Data Controller. The withdrawal will not
affect the lawfulness of the processing carried out before you have
withdrawn the consent.

You can exercise your rights by contacting the Data Controller, or in
case of conflict the Data Protection Officer. If necessary, you can also
address the European Data Protection Supervisor. Their contact
information is given under Heading 9 below.

Where you wish to exercise your rights in the context of one or several
specific processing operations, please provide their description (i.e.
their Record reference(s) as specified under Heading 10 below) in your
request.

## Contact information

- The Data Controller

If you would like to exercise your rights under Regulation (EU)
2018/1725, or if you have comments, questions or concerns, or if you
would like to submit a complaint regarding the collection and use of
your personal data, please feel free to contact the Data Controller,
<EU-CODE-EUROPA@ec.europa.eu>.

- The Data Protection Officer (DPO) of the Commission

You may contact the Data Protection Officer
<DATA-PROTECTION-OFFICER@ec.europa.eu> with regard to issues related
to the processing of your personal data under Regulation (EU)
2018/1725.

- The European Data Protection Supervisor (EDPS)

You have the right to have recourse (i.e. you can lodge a complaint)
to the European Data Protection Supervisor <edps@edps.europa.eu> if
you consider that your rights under Regulation (EU) 2018/1725 have
been infringed as a result of the processing of your personal data by
the Data Controller.

## Where to find more detailed information?

The Commission Data Protection Officer (DPO) publishes the register of
all processing operations on personal data by the Commission, which have
been documented and notified to him. You may access the register via the
following link: [http://ec.europa.eu/dpo-register](http://ec.europa.eu/dpo-register).

This specific processing operation has been included in the DPO's public
register with the following Record reference: DPR-EC-17414
