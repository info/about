# How to join About code.europa.eu project

## Why to join?
The project About code.europa.eu focuses on exchanging knowledge on using the platform and benefiting from different GitLab functions. 
It also aims at gathering users feedback, questions and suggestions how we can improve the portal for the benefit of the open source community.

Once you join, you will be able to browse through the wiki and submit questions and suggestions for improvements in the Issue section.

## How to request access and join?
Only registered users can become members of About code.europa.eu project. 

Visitors from outside of the European Union institutions can also register with EU Login. 
The registration is possible through the EU Login portal [here](https://code.europa.eu/users/sign_in). 
For more information see [About EU Login](https://ecas.ec.europa.eu/cas/about.html). 

Once you are logged in to code.europa.eu, head to the project [About code.europa.eu](https://code.europa.eu/info/about)
and click Request Access (see the screenshot):


![request_access](./request_access.png)
