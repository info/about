# Welcome to code.europa.eu

![European Commission logo](./european-commission-logo.png)

This is the code development platform for open source projects shared
by the institutions of the European Union. More precise, it is the
code development platform for open source software projects for which
European Union institutions, bodies, offices and agencies hold the
intellectual property rights.

Code.europa.eu was created following Commission Decision of 8 December
2021 on the open source licensing and reuse of Commission software
2021/C 495 I/01, which you can read
[here](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32021D1209(01)). It
is one of the outcomes of the Commission Open Source Software Strategy
C (2020) 7149 final. You can find the strategy and more information
about open source at the Commission
[here](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en).

## Public access

Start exploring projects [here](https://code.europa.eu/explore). All
of these projects are open to all visitors. Software developers who
wish to interact and collaborate with the development teams of the
projects: please, get in touch with these projects, this will help
with your registration on the platform.

## Project creation

On code.europa.eu, projects can **only** be created by software
development project teams working for European Union
Institutions. Dear colleagues, get in touch with us if you wish to set
up a new group/project (see below).

### Public project creation

The platform code.europa.eu is intended for open source projects
shared by the institutions of the European Union. The development
project teams can only create **Public projects**.

### Private or internal project creation exceptions

We understand that in some cases, project teams might require the
creation of private or internal projects. If this is the case, one of
the platform's administrators will create a project for you: please
[submit an issue
ticket](https://code.europa.eu/info/about/-/wikis/home#questions-feature-requests-issues)
in the Gitlab project [About
code.europa.eu](https://code.europa.eu/info/about/-/issues/new).
Please provide us with the right information, [described
here](https://code.europa.eu/info/about/-/wikis/Guidelines/How-to-request-private-or-internal-project-creation).

## Registration and sign-in

Software developers, project owners, and visitors from outside the institutions can sign in to the platform through the [EU Login Portal](https://code.europa.eu/users/sign_in).
For more information see [About EU Login](https://ecas.ec.europa.eu/cas/about.html)

**Note**: All users, regardless of their affiliation, must have their accounts validated by our staff before gaining access to the platform. 
We might contact you via e-mail if we require more information before validating your account (please check your e-mail's spam folder). You can find more information in [our FAQ here](https://code.europa.eu/info/about/-/wikis/FAQ#i-registered-but-im-getting-the-message-your-account-is-pending-approval-from-your-gitlab-administrator-and-hence-blocked-please-contact-your-gitlab-administrator-if-you-think-this-is-an-error).

## Join the project 'About code.europa.eu'

Registered users are invited to join this project (About
code.europa.eu) as members [(more information
here)](https://code.europa.eu/info/about/-/blob/master/join_About/request_access.md).
You have access to our wiki and know-how articles and you can view existing tickets, 
but membership is required to create and contribute to issues. 
You can also make suggestions on how to improve code.europa.eu and you can raise questions.

## Intellectual property

Developers who wish to create a project are advised to read the
[guidelines](./guidelines/code-europa-eu-guidelines.md).

For European Commission projects: your project must be declared in
Eureca, must go through the IP clearance and a security verification (see [guidelines](./guidelines/code-europa-eu-guidelines.md)). For questions about
Eureca, contact [Central IP Services](mailto:EC-IPR@ec.europa.eu).

![Guideliness for Projects going Open Source](./OSPO_Infographic_v4_small.jpg "Guideliness for Projects going Open Source"){width=35%}

## Security

Developers who wish to create a project, you are advised to read the
[guidelines](./guidelines/code-europa-eu-guidelines.md).

At a minimum, Commission projects must deal with:

1.  Secrets management (the code cannot include digital authentication
    credentials, including passwords, keys, APIs, tokens);

2.  Sensitive data management (the code cannot include information on
    internal infrastructure, domains, internal IP addresses).

For Commission projects, there are two services that can assist
in code security matters. For projects developed by the JRC, please
contact [JRC LISO](mailto:JRC-LISO@ec.europa.eu). All others, please
get in touch with to DIGIT S.

In addition to what is explained in the
[guidelines](./guidelines/code-europa-eu-guidelines.md), DIGIT S
offers static and dynamic code analysis. For dynamic code analysis,
project owners need to make available a test environment. Please get
in touch with DIGIT S. And read their wiki (link available on the EC
internal FPIS ThinkOpen wiki).

## Security and Vulnerability Scanning Options for Project Teams
Project's hosted on code.europa.eu can benefit from [Gitlab's Ultimate features](https://about.gitlab.com/pricing/ultimate/), including a variety of security scanning capabilities. They help the project teams in making sure their software is secure and free from vulnerabilities. 

The projects teams have at their disposal at least 2 different ways to scan for vulnerabilities  when it comes to using vulnerability scanning engines within code.europe.eu 
1) can use the **GitLab's Ultimate built-in security scans** and reports. You can read more about it [here](https://docs.gitlab.com/ee/user/application_security/secure_your_application.html/).
2) integrate your GitLab project with the **European Commission's in-house vulnerability scanning tools** and manage vulnerabilities and reports directly from GitLab. You can read [here](https://webgate.ec.europa.eu/fpfis/wikis/x/2AprG) on how to integrate your GitLab repository.

Project teams are advised to manage roles and permissions of their project's members in a way not to disclose vulnerabilities and vulnerability reports to third parties until the vulnerabilities are mitigated. You can read more about [Gitlab's Roles and Permissions here.](https://docs.gitlab.com/ee/user/permissions.html)

## Vulnerability Disclosure Policy

Any users or visitors of code.europa.eu who identify vulnerabilities related to the platform or any of the open source projects hosted here, are invited to report them as described in [the European Commission's Vulnerability Disclosure Policy](https://commission.europa.eu/legal-notice/vulnerability-disclosure-policy-vdp_en).

The project teams hosting their projects on code.europa.eu are also encouraged to inform their users about the [Vulnerability Disclosure Policy](https://commission.europa.eu/legal-notice/vulnerability-disclosure-policy-vdp_en) and make use of it for the purpose of their respective projects.

## Contribution

Projects receiving contributions from third-party developers, please
read our [recommendations](./dco-cla/intro.md) on how to manage
code contributions.

## Guidelines for contractors - legal compliance in developing open source solutions

Contractors or European Commission's teams working with contractors can refer to [these guidelines](https://code.europa.eu/info/about/-/blob/master/guidelines/guideliness_contractors_legal_compliance_open_source.md?ref_type=heads) that outline:

- the legal compliance process for open source software;
- the legal requirements for code repositories; and
- technical considerations for creating a repository and using code.europa.eu.

## Privacy

Code.europa.eu is a free service offered by DIGIT Unit B.2.

Please read our privacy statement,
[here](https://code.europa.eu/info/about/-/blob/master/privacy/code-europa-privacy-statement.md).

## CI/CD bring your own runners

For now, all groups/projects will have to bring their own CI/CD
Runners. 

(Note: in the future, the Commission might make available a few
default Runners.)

## Contact

Questions regarding code.europa.eu can be sent to the Commission open
source programme office; [EC OSPO](mailto:eu-code-europa@ec.europa.eu). You can also contact us here:
<https://ec.social-network.europa.eu/@EC_OSPO>. For media inquiries:
please contact the [DIGIT communication
team](mailto:digit-comm-team@ec.europa.eu).

