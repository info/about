# Usage Guidelines on PublicCode.yml Editor (v 1.4.3) from Interoperable Europe Portal 

These guidelines help projects hosted on code.europa.eu to generate their publiccode.yml file, using the dedicated [Editor](https://interoperable-europe.ec.europa.eu/publiccode-editor/). Read more about the publiccode.yml standard [here](https://code.europa.eu/info/about/-/blob/master/PublicCode.yml%20Standard/PublicCode_yml_standard.md).

This text is improved continuously, and we appreciate your feedback: [EU-CODE-EUROPA@ec.europa.eu](mailto:EU-CODE-EUROPA@ec.europa.eu)

Head to [https://joinup.ec.europa.eu/publiccode-editor/](https://joinup.ec.europa.eu/publiccode-editor/) to start generating your publiccode.yml file. You can use the same Editor to update your file  (see Updating your publiccode.yml file below)

## Change language to English
Before starting adding information in the Editor, we advise you to first change the default language, at the top. When you first access the Editor, the language is set to Italian; change this to English (en) and remove Italian (it) [see picture](https://code.europa.eu/info/about/-/blob/master/PublicCode.yml%20Standard/Screenshots/1-Language.png)

## 2.Repository & Documentation

### Repository URL
The URL path needs to point directly to the place where the code is stored. URLs such as https://code.europa.eu/GroupName/ProjectName will generate an error. The correct repository URL could be for example https://code.europa.eu/GroupName/ProjectName/-/tree/development

If you develop your project across multiple repositories (for example you have a repository for the front-end and a separate repository for the back-end),  choose the repository which is at the core of your project and which has the most complete documentation, which will also point the users to other repositories of your project.

## 3.Software Details

### Input Types / Output Types
Specify what type of media your software can process (input) and generate (output). Here is the [list of different Media Types (MIME Types)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types#types). These fields are not mandatory

## 4.Legal & Reuse

### License
Each public repository must contain a ‘Licence’ file indicating the licence under which the project is available (e.g. EUPL, MIT, GPL). For Commission projects, the licence must be chosen together with the Central IP Service. Please read the applicable guidelines [here](https://code.europa.eu/info/about/-/blob/master/guidelines/code-europa-eu-guidelines.md) .

### Codice iPA dell'ente (obbligatorio per Pubbliche Amministrazioni)
You can skip this section. It's specific to the Italian Public Administration only.

### Main Copyright Owner
For EU institutions and bodies under the EU personality (e.g. the European Commission, European Parliament, European Council, Council of the European, Court of Justice of the European Union, European Court of Auditors, European Economic and Social Committee, Committee of the Regions, European Data Protection Supervisor ) the copyright owner will be the European Union. (Commission projects: you can also mention the Directorate-General responsible for the software.)
 
Other institutions such as the European Investment Bank, the European Central Bank or Executive Agencies have separate legal personality and they are the owners of the project.

### Repository Owner
Same as above. In most cases this will be the European Union. (Commission projects: you may mention the Directorate-General responsible for the software project.

## 5.Description & Features

### Piattaforme
This is specific to the Italian Public Administration. You can skip this section.

### Conforme
This is specific to the Italian Public Administration. You can skip this section.

## 6.Logo & Screenshot

### Logo
The European Commission has a 'no logo rule' under which all Commission services, products, initiatives and activities should be identified solely by [the common visual identity](https://wikis.ec.europa.eu/display/WEBGUIDE/02.+Logos+and+icons). Projects should not use custom logo unless explicitly authorised.

However, the Commission's project can use the [logo](https://commission.europa.eu/resources-partners/european-commission-visual-identity_en) of the European Commission.

## 8. Maintainance

### Contacts
You can insert the functional mailbox address. Please DO NOT use CAPITAL letters in the e-mail address. Otherwise, the editor will generate an error when you will try to create the yml file. 

## Generate & Save your publiccode.yml file

When you're ready, click 'Generate' to create your yml file. In case of any errors, they will be shown in the right-hand side and you will be able to correct them and regenerate the file. 

Don't forget to download the file (it's not automatically downloaded). You have to click the download button in the right-hand section.

Upload the publiccode.yml file to your stable branch; don't change the file name!

## Update your publiccode.yml file

You can update your file at anytime. At minimum, you should update the file with each new release of your software.

Simply head to the Editor, upload your previous yml file. The fields should be automatically populated with data from your file. Update the fields you want to change the information and/or fill out new fields and generate the new file (don't forget to download it).

## Updating your publiccode.yml file
You can easily update your file by simply uploading the previously generated file, update the information in the editor and re-generate the yml file.