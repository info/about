# PublicCode.yml standard - promoting open source solutions across EU Public Services

## What is the PublicCode.yml standard
The publiccode.yml metadata standard is an established method for labelling
and describing a software solution.

The [publiccode.yml standard](https://yml.publiccode.tools/) was [created in Italy](https://developers.italia.it/en/reuse/publication), where it is mandatory for free and open source solutions created by the Italian Public Administrations to be listed in the Italian catalogue via a unique publiccode.yml record that describes the solution. It is a metadata file (built in YAML format/syntax) for making open source repositories easily discoverable and thus reusable by other entities. 

The [standard](https://yml.publiccode.tools/) specifies the structure of the yaml file and also which information have to be mandatory and optional. Details contained within a publiccode.yml file include:

* title and description of the project or product (in one or more languages);
* development state (e.g., concept, development, beta, stable, obsolete);
* contacts of the entity who published the codebase;
* information about the legal context for which the project or product was designed;
* dependencies and more.


## Why PublicCode.yml is important
By including a publiccode.yml file in the root of its own public GIT repository, and populating it with mandatory and other optional information, the software solution can be evaluated by technicians and civil servants. The file can be also automatically indexed and discovered by machines and interoperable applications around Europe and worldwide.

More and more online catalogs make use of information found in the publiccode.yml standard's file. One such example is the **EU Open Source Solutions Catalogue for European Public Services**, which is currently being developed by the European Commission. The Catalogue adopts the publiccode.yml metadata standard for the selection and the qualitative description of the OSS Solution in the context of software developed or acquired by and for European Public Services. 

A project must contain a file named publiccode.yml with the [information according to the standard](https://yml.publiccode.tools/), in order to be displayed in the **EU Open Source Solutions Catalogue** . All projects hosted on code.europa.eu are strongly encouraged to generate and maintain their publiccode.yml file in their repositories. 

## Publiccode.yml Editor
#### How to Create, Update & Validate your Own PublicCode.yml file
Creating, updating & validating your publiccode.yml file is pretty easy thanks to online dedicated Publiccode.yml Editors.
At moment we strongly suggest you to rely on (choose between) the following 2 ones:


1. **Developers.Italia Publiccode.yml Editor (2.x)**: https://publiccode-editor.developers.italia.it \
If you want to stay on track with its latest and more advanced version, in active development (and eventually face some possible minor bug)

2. **Interoperable Europe Portal Publiccode.yml Editor (forked from developers.italia 1.4.3)**: https://interoperable-europe.ec.europa.eu/publiccode-editor/ \
If you want to stay safe and traditional with a stable but older version, don’t need to translate into many languages and also don’t require latest improvements in the generation of it (such as fixed order generation).
Additional guidelines on using the Interoperable Europe Portal Publiccode.yml Editor (v 1.4.3) can be found [here](https://code.europa.eu/info/about/-/blob/master/PublicCode.yml%20Standard/publiccode_ymL_editor.md).

Those with more technical skills can also inspect and stay on track with latest progresses on [italia/publiccode-editor GIT repository ](https://github.com/italia/publiccode-editor).

## Publiccode.yml additional references:

* Official publiccode.yml Standard GIT repository: https://github.com/publiccodeyml/publiccode.yml
* Official publiccode.yml Editor GIT repository: https://github.com/italia/publiccode-editor 





