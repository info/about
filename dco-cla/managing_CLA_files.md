# Managing signed CLAs in EURECA

The signed CLAs need to be stored and archived for future reference.
Please save these files in EURECA under the Copyright file of the
software project, as follows:

## Step 1 - Filename

The signed CLA should have the following filename convention:
'CLA_NameOfProject_NameContributor_Year.pdf/doc'

## Step 2- Find Project in EURECA

Login to EURECA and search for your project. One of the search results
will be the blue Copyright dossier of your project (see screenshot).
Alternatively, the Copyright dossier can also be found at the bottom
of the page of your project's EURECA dossier. You
identify it by the blue widget with a © sign and a copyright number
in the format ‘COP000xxxx’.

![](./step1.png "step2.png"){width=50%}

## Step 3 - Upload the signed CLA

Access the ‘Action’ pane (1), then click on ‘Edit/ Update’ button (2)
and choose the ‘Documents’ (3) in the newly open box. Complete the
process by uploading the signed CLA file and entering a title
(recommended format: CLA_NameOfProject_NameContributor_Year). Do not
forget to click the ‘Submit’ button (6).

![](./step2.png "step2.png"){width=50%}
