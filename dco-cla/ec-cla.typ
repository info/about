#let color-ec = rgb("#4f81bd")

// code for 2nd level headlines 
// to add space between its auto-number and the headline term
#let numbers = (..nums) => {
  let n = nums.pos()
  
  if n.len() > 1 { 
    h(1.5cm)
    numbering("1.", ..n.slice(1)) 
    h(1cm)
  } else []

}

#set heading(numbering: numbers)

// Configure headings.
#show heading.where(level:1): it => [
  #align(center)[
    #text(fill: color-ec, size: 14pt, weight: "bold")[
      #upper(it)
    ]
  ]
]

// Configure headings.
#show heading.where(level:2): it => [
  #v(1em)
   #text(
    fill: color-ec,
    size: 10pt, 
    weight: "bold",
    it
  )    
  #v(.5em)
]

// Configure indent of numbered lists
#set enum(
  body-indent: 2em,
)

// justify all
#set par(justify: true)

// font
#set text(font: "Liberation Serif",
size: 9.9pt
)

// page numbering
#set page(
   numbering: "1",
)

= CONTRIBUTOR LICENCE AGREEMENT
\
Thank you for your interest in contributing to work developed by the
European Union (hereafter the "Project Owner", "We" or "Us").

== Definitions

"#strong[You]" means either the individual who Submits a Contribution to
Us or any Legal Entity on behalf of whom a Contribution has been
received by Us. "#strong[Legal Entity]" means an entity, which is not a
natural person. "Affiliates" means other Legal Entities that control,
are controlled by, or under common control with that Legal Entity. For
the purposes of this definition, "control" means (i) the power, direct
or indirect, to cause the direction or management of such Legal Entity,
whether by contract or otherwise, (ii) ownership of fifty percent (50%)
or more of the outstanding shares or securities which vote to elect the
management or other #strong[persons] who direct such Legal Entity or
(iii) beneficial ownership of such entity.

"#strong[Contribution]" means any work of authorship that is Submitted
by You to Us in which You own or assert ownership rights including, but
not limited to Copyright. If You do not fully own the entire work of
authorship, please ensure that a paper copy of this Agreement is signed
by all relevant copyright holders.

"#strong[Copyright]" means all rights protecting works of authorship
owned or controlled by You \[or Your Affiliates\], including copyright,
moral and neighbouring rights, as appropriate, for the full term of
their existence including any extensions by You.

"#strong[Work]" means the Software or the Media which are made available
by Us to third parties. After You Submit your Contribution, it may be
included in the Work.

"#strong[Software]" means any intangible asset resulting from a
development process and consisting of a computer program or a part
thereof.

"#strong[Submit]" means any form of electronic, verbal, or written
communication sent to Us or our representatives, including but not
limited to electronic mails, attachments, lists, source code control
systems, and issue tracking systems that are managed by, or on behalf
of, Us for the purpose of discussing and improving the Work, but
excluding communication that is conspicuously marked or otherwise
designated in writing by You as "Not a Contribution."

"#strong[Submission Date]" means the date on which You Submit a
Contribution to Us.

"#strong[Effective Date]" means the date You execute this Agreement or
the date You first Submit a Contribution to Us, whichever is earlier.

"#strong[Media]" means any Contribution or portion of a Contribution
which is not software: data, metadata, data bases, documents, manuals,
images, video, etc.

== Grant of Rights

#block[
#set enum(
  numbering: (..nums) => {
    set text(weight: "bold")
    let format = ("2.1", "(a)", "i.").at(nums.pos().len() - 1)
    numbering(format, nums.pos().at(-1))
  }
)
+ #strong[Licence Agreement:] You grant Us, and those who receive the
  Contribution directly or indirectly from Us, a perpetual, worldwide,
  non-exclusive, royalty-free, transferable irrevocable license in the
  Contribution with rights to sublicense through multiple tiers of sub
  licensees, to practice such rights, including, but not limited to, the
  right to reproduce, modify, prepare derivative works of, display,
  perform and distribute the Contribution and such derivative works;
  provided that this licence is conditioned upon compliance with Section
  2.3.

+ #strong[Patent Licence:] For patent claims including, without
  limitation, method, process, and apparatus claims which You \[or Your
  Affiliates\] own, control or have the right to grant, now or in the
  future, You grant to Us a perpetual, worldwide, non-exclusive,
  transferable, royalty-free, irrevocable patent licence, with the right
  to sublicense these rights to multiple tiers of sub licensees, to make,
  have made, use, sell, offer for sale, import and otherwise transfer the
  Contribution and the Contribution in combination with the Work (and
  portions of such combination). This licence is granted only to the
  extent that the exercise of the licensed rights infringes such patent
  claims; and provided that this licence is conditioned upon compliance
  with Section 2.3.

+ #strong[Outbound Licence(s):] As a condition on the grant of rights
  in Sections 2.1 and 2.2, We agree to license the Contribution under the
  terms of the initial licence of the project (including any right to
  adopt any future version of this licence).

  Without prejudice to the former conditions - and for the exclusive
  purpose of ensuring copyleft compatibility, We may licence the
  Contribution under any licences which are approved by the Open
  Source Initiative, or the Free Software Foundation, or Creative
  Commons, or equivalent non- proprietary licences published by the
  European Union on or after the Effective Date, including both
  permissive and copyleft licences, whether or not such licences are
  subsequently disapproved by relevant organisations (including any
  right to adopt any future version of these licences).

+ #strong[Moral Rights]. If moral rights apply to the Contribution, to
  the maximum extent permitted by law, You waive and agree not to assert
  such moral rights against Us or our successors in interest, or any of
  our licensees, either direct or indirect.

+ #strong[Our Rights]. You acknowledge that We are not obligated to
  use Your Contribution as part of the Work and may decide to include or
  not any Contribution We consider appropriate.

+ #strong[Reservation of Rights]. Any rights not expressly assigned or
  licensed under this section are expressly reserved by You.
]  

== Agreement: You confirm that:

a) You have the legal authority to enter into this Agreement.

b) You \[or Your Affiliates\] own the Copyright and any other
proprietary rights in the Contribution, including patent claims where
applicable, which are required to grant the rights as provided under
Section 2.

(c)(Individual) The grant of rights under Section 2 does not violate any
grant of rights, which You have made to third parties, including Your
employer. If You are an employee and you have created the Contribution
in connection with the performance of Your duties, You have had Your
employer sign this Agreement. If You are less than eighteen years old,
please have Your parents or guardian sign the Agreement on your behalf.

(c)(Entity) The grant of rights under Section 2 does not violate any
grant of rights, which You or Your Affiliates have made to third
parties.

(d) In case Your Contribution is based upon previous third party work,
such third party work is covered under an appropriate (open source)
6licence and can hence be used under the established licence of the
Work, and there are no incompatibilities arising from the use of the
Submitted Contribution within the Work;

(e) You will retain in Your Contribution all copyright and licence
information for any third party work that You use in or is associated
in Your Contribution. You will accompany Your Contribution by a
software bill of materials as required under good development
practices for open source projects (i.e. a list of third party work,
accompanied where necessary by the indication of whether such third
party work has been modified by You, third-party licences or other
restrictions).

(f) You communicated to Us a copy of this Agreement, signed by all
relevant copyright holders, if You do not own the Copyright in the
entire work of authorship Submitted.

== Disclaimer

EXCEPT FOR THE EXPRESS WARRANTIES IN SECTION 3, THE CONTRIBUTION IS
PROVIDED "AS IS". MORE PARTICULARLY, ALL EXPRESS OR IMPLIED WARRANTIES
INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NON- INFRINGEMENT ARE EXPRESSLY
DISCLAIMED BY YOU TO US \[AND BY US TO YOU\]. TO THE EXTENT THAT ANY
SUCH WARRANTIES CANNOT BE DISCLAIMED, SUCH WARRANTY IS LIMITED IN
DURATION TO THE MINIMUM PERIOD PERMITTED BY LAW.

== Consequential Damage Waiver

TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL
YOU \[OR WE\] BE LIABLE FOR ANY LOSS OF PROFITS, LOSS OF ANTICIPATED
SAVINGS, LOSS OF DATA, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL
AND EXEMPLARY DAMAGES ARISING OUT OF THIS AGREEMENT REGARDLESS OF THE
LEGAL OR EQUITABLE THEORY (CONTRACT, TORT OR OTHERWISE) UPON WHICH THE
CLAIM IS BASED.

== Miscellaneous

#block[
#set enum(
  numbering: (..nums) => {
    set text(weight: "bold")
    let format = ("6.1", "(a)", "i.").at(nums.pos().len() - 1)
    numbering(format, nums.pos().at(-1))
  }
)
+ This Agreement will be governed by and construed in accordance with
  the laws of Belgium.

  Any litigation resulting from the interpretation of this Agreement
  arising between the European Union and any Contributor will be subject
  to the jurisdiction of the Court of Justice of the European Union, as
  laid down in article 272 of the Treaty on the Functioning of the
  European Union.

  Any litigation arising between Parties, other than the European
  Union, and resulting from the interpretation of this Agreement, will
  be subject to the exclusive jurisdiction of the competent court
  where You reside or conduct your primary business.

+ This Agreement sets out the entire agreement between You and Us for
  Your Contributions to Us and overrides all other agreements or
  understandings.

+ If You or We assign the rights or obligations received through this
  Agreement to a third party, as a condition of the assignment, that
  third party must agree in writing to abide by all the rights and
  obligations in the Agreement.

+ The failure of either party to require performance by the other
  party of any provision of this Agreement in one situation shall not
  affect the right of a party to require such performance at any time
  in the future. A waiver of performance under a provision in one
  situation shall not be considered a waiver of the performance of the
  provision in the future or a waiver of the provision in its
  entirety.

+ If any provision of this Agreement is found void and unenforceable,
  such provision will be replaced to the extent possible with a
  provision that comes closest to the meaning of the original
  provision and which is enforceable. The terms and conditions set
  forth in this Agreement shall apply notwithstanding any failure of
  essential purpose of this Agreement or any limited remedy to the
  maximum extent possible under law.
]
\
\
A signed copy of this agreement will be considered as a valid
commitment.
\

\
#text(weight: "bold")[
  Name and Title (if applicable): #box(width: 30%, repeat[\_])
]

\
#text(weight: "bold")[
  Legal Entity (if applicable): #box(width: 30%, repeat[\_])
]

\
#text(weight: "bold")[
   Address: #box(width: 30%, repeat[\_])
]

\
#text(weight: "bold")[
    Date: #box(width: 30%, repeat[\_])
]

\
#text(weight: "bold")[
 Signature: #box(width: 30%, repeat[\_])
]
