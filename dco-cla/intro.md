# HOW TO HANDLE CONTRIBUTIONS FOR YOUR PROJECT
## Guidance for project managers of European Commission projects on code.europa.eu

To manage contributions from third parties, each and every project
shared by the European Commission on code.europa.eu should have in
place guidelines to describe how others may contribute to the project.
To help you with this, we - the Commission Open Source Programme
Office and the Commission Central IP Service - explain here what to
consider, and what to do.

### We distinguish three types of contribution:

1.  Minor code contributions
    
    For minor contributions like bug fixes and other minor code
    contributions, there is no need for the contributor to sign a
    particular document. It is sufficient that they signoff their
    commits with a GPG key.

2.  Contribution to documentation
    
    For contributions to documentation there is no need to request the
    contributor to sign a particular document. It is sufficient that
    they signoff their commits with a GPG key.

3.  All other code contributions
    
    For all other code contributions, you need to request the
    contributor to sign a Contributor Licence Agreement. This is
    necessary because code in general is protected by copyright, which
    belongs to the contributing individual or entity. In order for the
    EC to be able to use code written by third parties, we need to
    receive a legal right to do so.

### Contributor Licence Agreement

The Contributor Licence Agreement (CLA) grants an irrevocable right to
the EU to use the contribution, including to prepare derivative works,
sublicense, or distribute it. With a CLA, the contributors retain the
copyright to their contribution, and hence they remain free to use it
in any way they deem fit. They give the EU permission to use and
redistribute the contributions as part of the project. The permissions
under a CLA are granted perpetually, worldwide, royalty-free and
irrevocably.
    
A licence agreement is quite similar to a rental or lease agreement
where one can use a property, but does not own it.
    
Given that contributors retain their copyright ownership of the
contribution, they will be more likely to embrace this instrument
instead of an assignment of their copyright.

Contributors need to submit one CLA for each project they contribute
to.

### Add a Contributing file

Next, please make available a ‘Contributing’ file in the root
directory of your project, to make clear to contributors how to submit
their contributions. Please find
[here](./contributing-guidelines.md) the agreed Contributing
Guideline to be used by European Commission projects. Please read and
complete the text, before adding them to your project.

### Who should sign the CLA?

The answer to that question depends on who contributes. The following
questions will help you decide. You can also consult this diagram
(click on the image for a larger version): ![the image show a decision
tree diagram, with question in boxes with rounded corners, and a green
tick marks meaning yes, and red crosses meaning
no](./DecisionTree.png){width=50%}

### Is your contributor an EU Official?

Who owns the copyright in the contribution?
> The copyright belongs to the European Union.

What do they need to do?
> Sign their commits with a GPG key.

By “EU Official”, we mean any employee (permanent, contract or
temporary agent) of an EU institution under the EU personality:
European Commission, European Parliament, European Council, the
Council of the European, Court of Justice of the European Union,
European Court of Auditors, European Economic and Social Committee,
Committee of the Regions). Please note for instance that the European
Investment Bank, the European Central Bank or Executive Agencies have
separate legal personality and hence they should be included in
section b) below.

### Does the contribution come from public services inside or outside the European Union, other EU Institutions and Bodies with separate legal personality (Executive, Decentralized Agencies, EIB, ECB), universities, legal entities (NGOs, companies)?


Who owns the copyright in the contribution?
> The copyright belongs to the respective entity whether, an
> administration, an EU institution, university, company etc. (e.g ©
> Her Majesty The Queen in Right of Canada, Environment Canada, ©
> Helsingborg Stad, or © Open Knowledge Foundation)

What do they need to do?
> Entities: Sign the CLA and submit it to the project
>
> Employees of the respective entities: sign the commits made with a
> GPG key.
> 
> By signing the commit, the employee (individual) who submits a
> contribution indicates that he or she has read the terms of the CLA
> and agrees with these.

### Is the contributor a citizen, self-employed, or an employee acting outside the performance of their duties?

Who owns the copyright in the contribution? 
> The copyright belongs to the individual submitting the contribution.

What do they need to do?
> Sign the CLA and submit it to the project
> Sign their commits with a GPG key.

### Is your contributor an Individual acting in connection with the performance of his or her duties?

Who owns the copyright in the contribution?
> The copyright automatically belongs to his/her employer, except if
> the employment agreement explicitly stated otherwise (which normally
> never happens).

What do they need to do?
> Employer: Sign the CLA and submit it to the project.
>
> Employees of the respective entities: sign the commits made with a
> GPG key.
>
> By signing the commit, the employee (individual) who submits a
> contribution indicates that he or she has read the terms of the CLA
> and agrees with these.

### Who manages these contribution agreements?

The contribution agreements should be managed by project managers.

### Accepted signatures for agreements

-   electronic signatures using trusted service providers from the list
    available here:
    <https://eidas.ec.europa.eu/efda/tl-browser/#/screen/home>
-   wet signatures (with pen and paper) or
-   other eIDAS compliant digital solutions (e.g.
    [open-pdf-sign](https://www.openpdfsign.org) or [PDF
    Signature](https://github.com/24eme/signaturepdf).)

Contributions can only be accepted if the requirements above are met
in order to ensure legal certainty on its use.

### Managing signed CLAs in EURECA

The signed CLAs need to be stored and archived for future reference.
We explain how to do this, [here](./managing_CLA_files.md).

### IP clearance

Finally, when a contribution is adopted within a project, particularly
if such contribution contains third-party software, project owners
need to ensure that the use of such third-party software is consistent
with the overall licence of the project. 

If you have any doubts about this, please contact us (see below).

**Note:** In case you missed our reference above, you will want to read
the [Contributing Guideline](./dco-cla/contributing-guidelines.md).

For completeness sake: you can find more information on the
requirements of an IP assessment in the Software Distribution
Guidelines available
[here](./guidelines/code-europa-eu-guidelines.md).


### Questions?

If you have questions on the handling of a particular contribution,
please contact the [Open Source Programme
Office](<mailto:eu-code-europa@ec.europa.eu>) for IT or software related
issues, or [Central IP Services](<mailto:EC-IPR@ec.europa.eu>) for legal
or licence related issues.
