# How to Sign Git Commits with GPG in **code.europa.eu**

## Prerequisites

Before you start, ensure you have:
- Git installed on your machine
- GnuPG (GPG) installed
- A **code.europa.eu** account

## Step 1: Install GPG

### Linux
```sh
sudo yum install gnupg
or
sudo apt install gnupg
```

### macOS
```sh
brew install gnupg
```

### Windows
Download and install [Gpg4win](https://www.gpg4win.org/download.html).

## Step 2: Generate a New GPG Key
```sh
gpg --full-generate-key
```
Follow the prompts:
1. Select `RSA and RSA`.
2. Choose a key size (4096 is recommended).
3. Set an expiration date (or choose never to expire).
4. Enter your name and email (use the same email as your GitLab account).
5. Set a strong passphrase.

## Step 3: List Your GPG Keys
```sh
gpg --list-secret-keys --keyid-format=long
```
Find the key ID (e.g., `rsa4096/3AA5C34371567BD2`). Copy the part after `rsa4096/`.

## Step 4: Export Your Public Key
```sh
gpg --armor --export <KEY_ID>
```
Copy the output.

## Step 5: Add Your GPG Key to your Profile
1. Go to **code.europa.eu**.
2. Navigate to **Preferences** > **GPG Keys**.
3. Paste your public key.
4. Click **Add Key**.

## Step 6: Configure Git to Use Your GPG Key
### Automatically Sign All Commits
```sh
git config --global user.signingkey <KEY_ID>
git config --global commit.gpgsign true
```
This will sign all commits automatically.

### Manually Sign Individual Commits
Instead of configuring automatic signing, you can manually sign each commit using:
```sh
export GPG_TTY=$(tty)
git commit -S -m "Signed commit"
```
If you encounter issues, ensure the `GPG_TTY` environment variable is set before running `git commit`.

## Step 7: Test Signed Commit
```sh
export GPG_TTY=$(tty)
git commit -S -m "Signed commit"
```
If successful, you will see `gpg: Signature made ...` in the output.

## Step 8: Verify the Signature in **code.europa.eu**
After pushing your signed commit, check **code.europa.eu**’s UI. Your commit should show **Verified** if everything is correctly configured.

---