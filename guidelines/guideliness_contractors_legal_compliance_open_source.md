# LEGAL COMPLIANCE IN DEVELOPING OPEN SOURCE SOLUTIONS

Contents

[DISCLAIMER](#_Toc167438204)

[INTRODUCTION](#_Toc167438205)

[OPEN SOURCE REQUIREMENTS IN PROCUREMENT CONTRACTS](#_Toc167438206)

[THE INTELLECTUAL PROPERTY RIGHTS ASSESSMENT](#_Toc167438207)

[PREPARING THE REPOSITORIES](#_Toc167438208)

[CODE.EUROPA.EU](#_Toc167438209)

## DISCLAIMER

_Copyright European Union, 2024. All rights reserved._

_These guidelines are internal to the European Commission and should not be published or disseminated without prior permission; they are not intended to grant any rights or benefits to third parties and do not replace the need to seek professional legal advice. The legal opinions and analysis provided herein are without prejudice to the position which the European Commission may take in a particular situation and do not necessarily represent the official views of the European Commission._

_These guidelines are not exhaustive and do not exempt contractors from their obligations under the contract(s) signed with the European Union._

_Any obligations of contractors under the contract, particularly those to hold the Union harmless in case of infringement of intellectual property rights and to guarantee that the Union may exercise all modes of exploitation detailed in the contract, are fully applicable even if legal advice or additional assessments or verifications have been performed by any representatives of the Commission, including the Central IP Service, in the implementation of the contract by the contractors._

_The contractors remain free to choose and apply any policies, methods and tools they deem fit for producing the requested results._

## INTRODUCTION

This text provides guidance to contractors developing open source software for the European Commission. The document outlines:

1. the legal compliance process for such software;
2. the legal requirements for code repositories; and
3. technical considerations for creating a repository and using code.europa.eu.

This document gives the reader information additional to the 'Commission guidelines on software distribution', which can be found here: <https://code.europa.eu/info/about/-/blob/master/guidelines/code-europa-eu-guidelines.dm>

## OPEN SOURCE REQUIREMENTS IN PROCUREMENT CONTRACTS

The tender specifications to a Framework Contract or Service Contract for services may contain different provisions on the licensing of the software solutions to be developed. Some of these provisions may request the contractor to develop a solution that can be then licenced under open source terms, regardless of the outbound licence to be applied; other provisions might establish that the European Union Public Licence (‘EUPL’) is necessarily the open source licence under which the solutions will be licenced. Indeed, the EUPL is the preferred licence of the European Commission according to Commission Decision of 8 December 2021C(2021) 8759. In some cases however, another type of licence might be deemed more appropriate for a high added value technology artefact.

Ideally, the contractor should refer early on in the development process to any specific guidance in order to ensure that no legally conflicting externally-sourced components are included in the end-product. If the contractor did not take the necessary measures in the coding process, this means that the assessment may become more burdensome, particularly if the contractor did not keep accurate records on the origins and licences of externally-sourced elements.

In case the procurement documentation does not favour a particular open source licence such as the EUPL, the contractor has more liberty in choosing the components to be used in the development process. Of course, the contractor still needs to conduct a licence review process to ensure licence compatibility.

In order to be able to license a software solution under the EUPL it is necessary to calibrate the choices for third party components in the development. Such choice will, depend among others, on third party components’ respective licences, as some licences might not be compatible with the EUPL. Some applicable guidance:

- The third party components used should be covered by open source licences compatible with the EUPL. To this end, the contractor may consult the matrix of EUPL compatible open source licences available on [Joinup](https://joinup.ec.europa.eu/collection/eupl/matrix-eupl-compatible-open-source-licences). In principle, for a distribution under the EUPL, it is not possible to make use of externally-sourced components under a strong copyleft licence (e.g. GPL 2.0, GPL 3.0 or AGPL 3.0) or under share-alike licences (e.g. ‘SA’ Creative Commons licences), unless these licences (i) are part of a dual/ multi licensing scheme alongside EUPL-compatible licences (e.g. EPL, CDDL), (ii) introduce any particular exceptions (e.g. classpath exception) or (iii) the GNU’s principles of communicating ‘at arms length’ apply<sup>[\[1\]](#footnote-1)</sup>.
- Additionally, distribution under EUPL is not possible whenever the final solution makes use of or is a derivative work of components whose licence terms prohibit their incorporation into a copyleft-licenced solution (e.g. the notion of copyleft licence is described under “_excluded licence_” in the MICROSOFT .NET library License Terms).
- The use of components under licences which are not generally recognised by open source organisations is not allowed (e.g. SSPL, Elastic, BSL, Json, NC’ Creative Commons licences etc.);
- Proprietary licences cannot be used unless a special derogation for their use in open source projects is obtained from the software owners/ providers.

## THE INTELLECTUAL PROPERTY RIGHTS ASSESSMENT

<table><tbody><tr><th><p><strong>Key aspects:</strong></p><ul><li>Source code</li><li>Binaries</li><li>Datasets/ databases</li><li>AI models</li><li>Fonts, icons, images, text</li></ul></th><th><p><strong>Key activities:</strong></p><ul><li>the identification of all third party code dependencies used (direct and transitive)</li><li>the identification of all third party non- code dependencies</li><li>the gathering of additional metadata (e.g. involved licences and copyrights)</li><li>the execution of checks on the assembly of components and licences</li></ul></th></tr></tbody></table>

- In order to perform an IP assessment, the contractor is expected to maintain a list of all direct and transitive dependencies of the produced code and their licences, accompanied where necessary by the indication of whether such dependencies have been modified (ideally via a software bill of materials – ‘SBOM’ as required under good development practices for open source projects).
- The list of dependencies (or the SBOM) can be generated using a code scanning tool, a package manager or a dependency management tool. If the software has been developed in a language which allows the use of package managers (such as Maven in Java), the contractor can extract and provide a detailed list of open source components and dependencies, which will facilitate the legal assessment process. However, the automatically generated lists must always be double-checked and, if necessary, complemented. The contractor may make use of [the SBOM template here](https://code.europa.eu/info/about/-/blob/master/guidelines/files/Template_Dependencies1.ods).

<table><tbody><tr><th><p><strong>Non exhaustive examples</strong>:</p><p>Package managers:</p><ul><li>Python: pip (<a href="https://pypi.org/project/pip-licenses/">pip-licenses</a>); poetry</li><li>Java: License Maven Plugin</li><li>Javascript: <a href="https://www.npmjs.com/package/license-checker">license-checker</a>, <a href="https://www.npmjs.com/package/license-report">license-report or </a><a href="https://www.npmjs.com/package/licenses-list-generator">licenses-list-generator</a>, <a href="https://github.com/jk1/Gradle-License-Report">Gradle License Report</a></li></ul><p>Other open source (e.g. SCANCODE, FOSSA, ORT, REUSE etc.) or proprietary tools<sup><a href="#footnote-2" id="footnote-ref-2">[2]</a></sup>.</p></th></tr></tbody></table>

- **Cross-compatibility check**: After having obtained a complete overview of all the licences, it is necessary to conduct a cross-compatibility check of (copyleft) licences, paying particular attention to a potential incompatibility between different version numbers of the same licence family (e.g. GPL 2.0 and GPL 3.0). Indeed, licences can contain contradictory or highly restrictive requirements, making it impossible from a legal point of view to combine source code from separately-licensed software in order to create and distribute a new program.
- **Inbound – outbound**: The combination of components, each governed by a certain licence, often determines the licence of the final work that incorporates such components. For this reason, the combination of inbound licences must allow the application of the established open source licence (e.g. EUPL or a permissive licence).
- **Binaries vs sources**: The assessment should take into account the type of software that is being used – i.e. binaries or sources; if the components have been used in source code, it should be considered whether there were any modifications made to such source code (some licences may display different legal effects in case a modification has been performed on the source code).
- **Linking method**: Special focus must be placed on the way the third party components are being linked/ ‘called’ – a static linking may produce different effects from a dynamic linking (e.g. the LGPL). Nevertheless, for the purpose of the assessment it should be considered that both a static and a dynamic linking produce a derivative work.
- **Container technologies**: In the case of container technologies (e.g. docker), if there are open source or proprietary components built into containers or wrapped without a package manager, the contents of the containers should be verified to ensure licence compatibility.
- **Access to the source code**: Under good development practices, the IP assessment should also be based on access to the source code (to check, for instance, for inaccurate copyright notices, potential non declared dependencies etc.).
- **AI models and training data**: The IP assessment should also identify any AI models and training data to evaluate whether three are any limitations or obligations resulting thereof. If the AI models or datasets will be redistributed with the software solutions developed, their licensing terms must be consistent with the distribution strategy.
- **Compiler**: For the distribution of compiled code / object code, the compiler used should also be assessed. Most compilers usually inject certain object files and binary elements into their end-product, for a variety of technical reasons. Most of such injections do not give rise to pertinent IPR consequences; however, licensing terms of some compilers may include restrictions or conditions on the downstream licensing of the end-product. Therefore, when the Commission also intends to license the object code or executable version of a software asset, the contractor should also look at the licensing terms of the compiler used, in order to ascertain whether the choice of compiler has any IPR repercussions.
- **Non-code components**: Non-code components (fonts, icons, text, images, data) must not be neglected, particularly if these will be distributed alongside the codebase.
- **AI-generated code**: Use of AI-generated source or object code poses several risks such as introducing unsecure or even malicious code or infringing copyright by introducing pre-existing source code or object code from third-parties (e.g. if copyrighted source code or object code was used to train the model and ends up being replicated (quasi) identically in the output.). For this reason, AI-generated code should be thoroughly assessed in order to ensure that it does not include any third-party code whose use would infringe the licence under which such code was made available.

# PREPARING THE REPOSITORIES

To prepare the repositories for publishing, the following should be considered:

- The root directory must contain a ‘**Licence’** file with the text of the outbound licence chosen for the software solution.
- The root directory must contain a ‘**Notice’** file whose purpose is to ensure compliance with the obligations arising from the different open source licences used (e.g. attribution obligations, obligations to state the licence, to give a copy of the licence, to state modifications or to give a copy/ link to the source code etc.). The Notice file should contain:
- The reference to the ownership of the European Union on the software as follows: “© European Union 20XX \[year of first publication\]”;
- The header of the licence chosen for the distribution of the software solution, if any (e.g. EUPL, GPL, Apache have standard licence statements);
- Any copyright or other notices for the third party dependencies used, the reference to the licences of these dependencies, the links to the source repositories, etc.<sup>[\[3\]](#footnote-3)</sup> In principle, copyright/ attribution notices are included in the (git) repositories of each component in different locations: either in their ‘_Licence’_ file or in other files such as ‘_Notice’_ or ‘_Copying’_, but also in the headers of the source files. Some examples are available [here](https://code.europa.eu/leos/core/-/blob/development/NOTICE_FE.md?ref_type=heads) or [here](https://code.europa.eu/web-t/web-t-translation-hub-containerable/configuration-service/-/blob/main/Notice.txt?ref_type=heads). A solution for the generation of the Notice file is available [here](https://code.europa.eu/leos/core/-/tree/development/tools/license-notice-file-generator?ref_type=heads.). However, this solution must necessarily be accompanied by additional verifications and manual work.
- A short copyright notice should be included at the **top of each source file**. Example:

_“ /\*_

_Copyright (c) 2024 European Union_

_\*_

_Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the_

_European Commission – subsequent versions of the EUPL (the “Licence”);_

_You may not use this work except in compliance with the Licence._

_You may obtain a copy of the Licence at:_

_\*_

[_https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12_](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)

_\*_

_Unless required by applicable law or agreed to in writing, software distributed under_

_the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS_

_OF ANY KIND, either express or implied. See the Licence for the specific language_

_governing permissions and limitations under the Licence._

_\*/._

# CODE.EUROPA.EU

Code.europa.eu is a GitLab-based platform operated by the Open Source Programme Office (‘OSPO’) at DIGIT. All of the EU Institutions are encouraged to use it as the default platform for sharing their open source projects with the public, the Member States and other organisations. More information about the platform can be found on the following pages:

- [General information](https://code.europa.eu/info/about)
- [Wiki](https://code.europa.eu/info/about/-/wikis/home)
- [FAQ](https://code.europa.eu/info/about/-/wikis/FAQ)
- [How to handle contributions for your project](https://code.europa.eu/info/about/-/blob/master/dco-cla/intro.md?ref_type=heads)

In case of any questions, please contact DIGIT OSPO: [EU-CODE-EUROPA@ec.europa.eu](mailto:EU-CODE-EUROPA@ec.europa.eu).

**Who can create a repository and use code.europa.eu?**

An open source project repository on code.europa.eu can be created by anyone with the \*europa.eu email address, including contractors who have the LDAP email address from an EU Institution (for example ext.ec.europa.eu).

In order to be able to create a repository, a user needs to login to code.europa.eu using their EU Login account and inform DIGIT OSPO ([EU-CODE-EUROPA@ec.europa.eu](mailto:EU-CODE-EUROPA@ec.europa.eu)). DIGIT OSPO will then grant the user the rights to create a repository. Please note that the Commission-led projects need to go through the IP clearance, EURECA<sup>[\[4\]](#footnote-4)</sup> registration and vulnerability assessment prior to being shared on code.europa.eu

The user who creates a repository will be the owner of such repository. If necessary, s/he can share the ownership or transfer it to another user with the LDAP account of an EU Institution.

The owner(s) can manage the access rights to the repository and grant the necessary Permission and Roles to other users, including contributors and contractors who do not have an LDAP account of an EU Institution. Therefore, external contractors without the LDAP account, Members States as well as the public can contribute to the project and even be a member of a project/repository as long as the ownership and oversight remains in the EU Institution.

**When to create a repository?**

Project’s repository can be created on code.europa.eu at any moment of a project’s life-cycle. It can be created at the very early stage of the project before any code is written or when the source-code and the project are mature.

In case of the former when the project only starts, we recommend the project team to have an initial consultation with the Central IP Service team in order to discuss possibilities for the open source licence. The project team also needs to be aware and plan for the vulnerability assessment throughout the life-cycle of the project. Registering the project in EURECA is mandatory.

When a project already has the source code and wants to share it, then the project team needs to register it in EURECA, go through the IP Clearance process and ensure no vulnerabilities and/or secrets are in the code prior to creating the repository.



1. The work under development and the GPL component(s) cannot be combined in a way that would make them effectively a single program. For more information, refer to the explanation here: <https://www.gnu.org/licenses/gpl-faq.en.html#GPLInProprietarySystem> [↑](#footnote-ref-1)

2. For additional tools see the study here: <https://op.europa.eu/en/publication-detail/-/publication/807bc813-9c04-11eb-b85c-01aa75ed71a1/language-en> [↑](#footnote-ref-2)

3. It is the responsibility of the contractor to check the obligations for each (open source) licence used and to ensure that they are all reflected in the Notice file. [↑](#footnote-ref-3)

4. EURECA is the European Commission’s internal IP catalogue and management platform. [↑](#footnote-ref-4)