# Guidelines on Commission software distribution


-   [Disclaimer:](#disclaimer)
-   [Introduction](#introduction)
-   [The European Commission\'s policy on open
    source](#the-european-commissions-policy-on-open-source)
    -   [Main takeaways from the Software Reuse
        Decision](#main-takeaways-from-the-software-reuse-decision)
-   [Distribution of EC software](#distribution-of-ec-software)
    -   [Applicable workflow](#applicable-workflow)
    -   [The concept of distribution](#the-concept-of-distribution)
    -   [Procedure in case of open source
        distribution](#procedure-in-case-of-open-source-distribution)
    -   [Procedure in case of proprietary
        distribution](#procedure-in-case-of-proprietary-distribution)
    -   [Preparing the software for
        distribution](#preparing-the-software-for-distribution)
-   [Software development process](#software-development-process)
    -   [Planning ahead in the development
        lifecycle](#planning-ahead-in-the-development-lifecycle)
    -   [Types of development projects](#types-of-development-projects)
    -   [Choosing suitable components](#choosing-suitable-components)
    -   [Keeping records](#keeping-records)
    -   [Suitable clauses in the procurement
        contracts](#suitable-clauses-in-the-procurement-contracts)
    -   [Limitations of open source licences: conditions and licence
        compatibility](#limitations-of-open-source-licences-conditions-and-licence-compatibility)
    -   [Distribution of object code and the use of
        compilers](#distribution-of-object-code-and-the-use-of-compilers)
    -   [Including the EU copyright notice and the licence under which
        the software is
        distributed](#including-the-eu-copyright-notice-and-the-licence-under-which-the-software-is-distributed)
    -   [Third-party notices and
        disclaimer](#third-party-notices-and-disclaimer)
-   [Resources](#resources)
    -   [Information and materials on intellectual property and open
        source](#information-and-materials-on-intellectual-property-and-open-source10)
    -   [Security information](#security-information11)
    -   [Joinup](#joinup)
    -   [EUPL](#eupl)
    -   [Contact points](#contact-points)
-   [Footnotes](#footnotes)

## Disclaimer:

These guidelines are internal guidelines of the European Commission;
they are not intended to grant any rights or benefits to third parties,
nor to provide legal advice. The legal opinions or analysis provided
herein are without prejudice to the position which the European
Commission could take in any particular situation and do not necessarily
represent the official views of the European Commission.

## Introduction

Software developed by different services of the European Commission
(\"EC" or "the Commission") as part of their work is the property of the
European Union (article 18(1) of the Staff Regulations). The same
applies to software developed for the EC by external contractors (*intra
muros or extra muros*), if procured using the standard contract models
approved by DG BUDG.

The Commission can decide to make such software available to the
community or to specific third parties. To minimise legal risks
associated with the future distribution of a software, when the purpose
is to distribute the software under an open source ("OS") or a
proprietary licence, its development should follow the current
guidelines ("the Guidelines").

The Guidelines only apply to software developed by or for the EC and/or
for which the Commission holds the intellectual property ("IP") rights
on behalf of the EU.

In these Guidelines, as also in the Software Reuse Decision, when it is
stated that a decision may be taken or a choice may be made by "EC
services"[^1], it means that the decision must then be taken in
accordance with the rules or practices that apply in that particular
service and in the DG concerned: for instance, some important choices
may need to be made only by the Director General or a by a director (or
with their consent), while less significant choices (in terms of
strategy, of costs involved, etc.) might be taken at the level of the
Heads of Unit; in other words, there are no specific rules which apply
here and which would differ from the rules applying to other projects,
decisions or policy choices.

## The European Commission\'s policy on open source

On 21 October 2020, the Commission adopted a new strategy on open source
software[^2], encouraging the use of open source software by the
Commission, the Commission\'s contribution to third-party open source
projects and the sharing of Commission software as open source. It was
set forth that, wherever possible and appropriate, the Commission would
share the source code for any computer programs where it holds the
intellectual property rights on behalf of the EU.

In line with the open source strategy, on 8 December 2021, the
Commission adopted a Decision[^3] on the open source licensing and reuse
of Commission software ("the [Software Reuse
Decision](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32021D1209%2801%29)"),
setting the framework for the distribution of Commission software as
open source. The Software Reuse Decision entered into force and is
applicable as of the date of its adoption.

### Main takeaways from the Software Reuse Decision

1.  EC services that develop software remain free to decide whether to
    license it under an open source licence or under a proprietary
    licence or not to distribute at all.

    1.  If the software is made available under **an open source
        licence**,

    the following rules apply:

    -   The default open source licence should be the European Union
        Public Licence ("the EUPL") except where:
        -   the use of another open source licence is obligatory due to
            the use of third party software under copyleft licences
            (e.g. GNU GPL);
        -   where an alternative open source licence is deemed
            preferable to the EUPL, notably in order to facilitate its
            adoption by the community of users, provided it is a
            standard open source licence.
    -   The software should be made available in source code and, where
        appropriate, in compiled code in a trusted repository of the
        Commission.
    -   Making software available under an open source licence does not
        oblige EC services to:
        -   adapt or update[^4] the software;
        -   translate the software and related information into any
            language versions other than those already available in the
            repository;
        -   continue the development or storage of the software or to
            preserve the software in a given format;
        -   set up or support a community of users of the software.
    -   The required preliminary steps to OS distribution include:
        -   a software identification process;
        -   an IP verification (as laid down by the Software Reuse
            Decision, further referred to in these Guidelines as "\*IP
            clearance\*");
        -   a security verification (as laid down by the Software Reuse
            Decision, further referred to in these Guidelines as
            "\*vulnerability assessment\*").

    1.  If the software is made available under a **proprietary
        licence,**

    to one or several selected licensees, possibly against the payment
    of royalties, such distribution requires the adoption of a
    Commission Decision under the IP delegation ((SEC(2001) 1397). The
    decision to distribute under a proprietary licence should be
    justified by public policy interests, policy reasons or technology
    transfer considerations, or by cases where it is necessary to
    provide a service in the public interest.

2.  **Commission services are allowed to take part in and contribute to
    external open source projects** deemed to be in the interest of the
    EU or of one of its policy objectives. If required by the rules
    applying to the project, ownership of the intellectual property
    rights on the contributed software may be transferred, without a
    Commission Decision[^5], to the public or private entity in charge
    of those open source projects.

3.  **The Decision does not apply:**

    1.  to software for which the Commission is not in a position to
        allow reuse due to the IP rights of third parties;

    2.  to Commission software where the publication or sharing of its
        source code would present an actual or a potential risk to the
        security of the information systems or databases of the
        Commission or of another European institution, agency or body;

    3.  to Commission software that must be considered confidential in
        accordance with applicable rules or legislation, contractual
        obligations, or as a result of its nature or contents;

    4.  in cases where, due to one of the exceptions listed in Article 4
        of Regulation (EC) 1049/20016 mutatis mutandis, the Commission
        software must be excluded from access or can only be made
        accessible to a party under specific rules governing privileged
        access;

    5.  to Commission software resulting from ongoing research projects
        conducted by the Commission or on its behalf, which are not
        published and where publication would (i) interfere with the
        validation of provisional research results or (ii) constitute a
        reason to refuse registration of industrial property rights in
        the Commission\'s favour, where such registration is deemed to
        be appropriate.

## Distribution of EC software

As the use and distribution of software which contains pre-existing
components for which insufficient rights have been secured may lead to
copyright infringement (and reputational and financial damages),
software must always be subject to identification and IP clearance
before distribution. Also, software must undergo a **vulnerability
assessment**.

The following conditions and workflow apply to all software which is
intended for distribution and has not been licenced before, regardless
of when it was developed -- before or after the adoption of the Software
Reuse Decision. They also apply to software which has been distributed
before under a particular licensing scheme and EC services consider it
appropriate to change this scheme, for example by moving from limited
proprietary distribution to open source distribution.

Existing software for which a licensing scheme has already been
established before the adoption of the Software Reuse Decision does not
need to be re-submitted to the new rules of the Software Reuse Decision:
the previously chosen licence continues to apply. However, if the
development of such software was further pursued and EC services
consider that significant changes have been made that could impact the
existing licensing scheme (for example by including or linking to OS
components under copyleft licences), EC services can ask the Central IP
Service to assess these changes.

### Applicable workflow

The software distribution workflow is summarised in image 1. The steps
are elaborated in the text below.

[![](img/software-distribution-workflow.svg)](file:img/software-distribution-workflow.svg)
<figcaption>
image 1
</figcaption>

#### Register in Eureca (CIPS):

The EC Service developing the software completes a new software dossier
in EURECA (the EU IP catalogue
and management platform). This initial entry lists, as a minimum, the
contact person and unit, the title of the asset, a short description and
the software bill of materials (comprehensive list of the third-party
components and dependencies)

#### Request IP Clearance and advise on distributions:

If the EC Service wishes to distribute the software, it has to request
an IP clearance and a vulnerability assessment, by providing access for
the corresponding services to the source code and executable version of
the software. For the **IP clearance**, the EC Service sends a
request to the Central IP Service at their mailbox EC-IPR@ec.europa.eu.

The Central IP Service assigns the file to a legal officer, who will
carry out an IP clearance based on the information provided through the
bill of materials and additional clarifications from the EC Service.

Depending on the result of the IP clearance, the legal officer informs
the EC Service and advises on the potential distribution of the
software:

If the EC Service intends to distribute the software under a free and
open source licence:

-   The legal officer confirms that there are no legal obstacles to
    distribution in line with the Software Reuse Decision.

If the EC Service intends to distribute the software under a proprietary
licence:

-   The Director General of the originating Service adopts a Commission
    decision under SEC(2001)1397.

The EC Service can proceed to make the software available according to
the agreed licensing terms - open or closed source.

#### Perform or request vulnerability assessment

For the **vulnerability assessment**, the EC Service sends a request to the
Security Assurance team in DIGIT.S1 \[via (MITS)\]. Alternatively, the
vulnerability assessment can be carried out directly by a Commission
department, if it has the dedicated expertise and resources for
performing this task. All such departments will be explicitly
identified, as far as is possible. If there is a doubt about which
department is responsible, the EC Service should contact the Open Source
Programme Office at eu-code-europa@ec.europa.eu.

Further information on identification, IP clearance and vulnerability
assessment are provided in the following sections.

------------------------------------------------------------------------

-   As a general note, all EC software should be identified in EURECA
    and be subject to an IP clearance, regardless of the dissemination
    policy - distribution under open source/ proprietary terms or SaaS
    (\'software as a service\') distribution. Likewise, the
    vulnerability assessment should be an integral step throughout the
    entire software development process.
-   You are recommended to request IP clearance and a vulnerability
    assessment from the relevant services towards the final phases of
    the development, when your EC service has a clear representation of
    the final software composition. In this way, the evaluation of
    security and licence compliance will properly reflect the risks and
    authorisations deriving from the (OS) components used.

------------------------------------------------------------------------

### The concept of distribution

Distributing software means providing copies of it for the public (in
source and/or object code) under certain licensing terms. Any Commission
software that is distributed should always be accompanied by a licence
defining the terms and conditions for its use. The applicable procedure
for this is determined by the type of distribution that you choose.

There are 4 options for software distribution:

-   Distribution under an **open source licence**;

-   Distribution under a **proprietary licence** (granting individual
    licences or an end-user licence, either free of charge or in return
    for a licensing fee);

-   **SaaS distribution** (\'software as a service\') -- software is
    made available to the public on a server connected through a network
    interface.

-   **No distribution.**

#### Open source distribution

EC software strategy follows the openness principle, whereby software
developed by or for the Commission should be made available to the
public for further development and use. By emphasising openness, the
Commission has made open source distribution the default policy for the
code it produces.

Open source makes use of copyright to enable the widespread
dissemination of software code and incentivise further development. An
inherent characteristic of all open source licences is that the software
must (also) be distributed in its source code form.

Recipients of software under any open source licence can exercise the
following rights ("the four freedoms"): *use, copy, modify, and
distribute* the software (with or without modifications). _[These rights
are common to all open source licences]_

There are many types of open source licences created by different
organisations (e.g. Massachusetts Institute of Technology, the Free
Software Foundation (FSF), the Apache Software Foundation). The most
popular sets of open-source software licences are those approved by the
FSF, based on the "[free
software](https://www.gnu.org/philosophy/free-sw.html)" criteria or
those approved by the [Open Source
Initiative](https://en.wikipedia.org/wiki/Open_Source_Initiative) (OSI)
based on their [Open Source Definition](https://opensource.org/osd).

However, the different open source licences also contain terms and
conditions that differ from one licence to another. For this reason, not
all open source licences are the same and, for various reasons, some may
not be compatible.

Traditionally, a well-established categorisation exists between
*permissive* and *copyleft* open source licensing.

------------------------------------------------------------------------

**Permissive open source software licences** are designed to enable the
sharing of code with the fewest possible legal encumbrances. The
licensor adopting these licences does not seek to control how modified
code is relicensed by third parties and choses to apply only minimal
requirements to its dissemination. The requirements often consist only
of retaining information about the author(s) and their copyright notice
(i.e. attribution)

Thus, under the terms of a permissive open source licence, the recipient
of the licenced code can modify it and redistribute the resulting
derivative work under different licence terms - including closed-source
proprietary terms.

Examples: MIT, BSD 3-Clause, Apache, Open Font.

**Copyleft open source licences** are designed to keep the software open
source. They do this by including a copyleft clause, which is an
obligation triggered when the software is distributed and that requires
any subsequent distribution to be done under the same (or compatible)
copyleft terms. In other terms, the copyleft clause generates a *viral
effect* that extends to any additional code that is incorporated with
the original copyleft code. The licensor adopting a copyleft licence
seeks to ensure that the released source code and any modifications to
it will remain free and publicly available.

Depending on their effects, copyleft licences can be categorised as
follows:

-   **Strong copyleft licences** are designed in such way that any
    modification, derivative work, or distribution obliges the entire
    new software to be licensed under the same terms. If EC software
    incorporates external elements which correspond to more than one
    kind of such licence, then no distribution is possible because they
    are mutually exclusive.

    Examples: GPL 2.0, GPL 3.0, AGPL.

-   **Weak copyleft licences** (mostly convenient for tools or
    libraries) have wider compatibility than strong copyleft licences
    and only the original work and its *direct* modifications inherit
    the licence. The licence is not extended to all combined or
    resulting derivatives, even those including code generated by the
    tool when producing works. Such resulting products can be licensed
    through any other licence (open or proprietary). However they may
    place different limitations on downstream distribution (e.g.
    exempting only executable versions from the viral effect)

    Examples: LGPL, MPL, EPL, CDDL.

------------------------------------------------------------------------

#### Distribution under proprietary terms

Should an open source distribution strategy not be suitable or possible,
then a distribution under proprietary terms may be considered. Under
proprietary licences, licensors are free to shape the terms and
conditions in the licence as they see fit for their particular purpose.

Unlike an open source distribution, a proprietary distribution is often
closed source: the software is either distributed in its executable form
and users merely execute the software (i.e. end-user) with no access to
its source code. It might be the case as well that access to the source
code is provided, often with restrictions on use (e.g. access to the
source code is granted only for the purposes of development and
maintenance, and any modification of the source code requires prior
written consent).

Particular attention should be paid to the inclusion of third party
components in software which is intended to be kept under proprietary
terms. For instance, the use of code available under strong copyleft
licences (such as GPL) is not possible. The GPL licensing terms
expressly prohibit incorporating programmes under the GPL licence into
proprietary software. If licensing under proprietary terms is envisaged,
the components covered by the GPL licence need to be removed and/or
replaced by components under permissive or weak copyleft licences.

#### SaaS distribution

Software as a service (SaaS), sometimes called web-based software,
on-demand software, hosted software or cloud-based software, is a way of
delivering applications over a network, as a service. This model does
not involve any distribution, as users do not get a copy of the software
in either source code or executable code. Instead of installing and
maintaining software, the user simply accesses it via a network.

SaaS is usually provided to external parties with a service level
agreement or under specific terms and conditions, without requiring the
software to be licensed out, as it is not formally distributed. However,
even if the software is made available under the SaaS model, it can be
the case that EC services can decide to make the software available as
open source or under proprietary terms or be obliged to distribute the
source code under an open source licence.

Particular attention must be given to the use of third party software
covered by certain licences such as EUPL or AGPL -- these licences
consider the use over a network to be equivalent to distribution, which
has the consequence of triggering the related effects, including that of
disclosing the source code.

**Any time there is a doubt on the use and effect of copyleft licences,
you should consult the Central IP Service.**

All decisions not to distribute the software in source/ binary code need
to be motivated under the responsibility of the Head of Unit.

#### No distribution

It might be the case where there is no distribution intended and the
software is only developed and used for internal purposes, without being
made available for the public. In such cases, the risk associated with
the use of third-party components in the development process is lower
than when distribution is planned.

### Procedure in case of open source distribution

According to the Software Reuse Decision, in the event of open source
distribution, the following steps should be followed:

#### STEP 1 - Identification

------------------------------------------------------------------------

Identification in EURECA is a **mandatory step** before the IP clearance
and security assessment are carried out by the corresponding EC
services.

The identification process should be carried out by a staff member of
the EC service responsible for the software project (ideally, the
project manager or lead developer).

To do so, they create a new dossier in
EURECA. This will open an
individual file, allocate a record number (ID) and create an initial
entry in the Commission\'s inventory of IP assets. This initial entry
lists, as a minimum, the contact person and the unit of origin, the
title of the asset and a short description.

To follow the Commission\'s governance practices[^6], after the EURECA
ID is created, EC services (the system supplier/provider) are required
to fill out the Intellectual Property Rights Reference (EURECA ID) in
GovIS2\'s IS sheet.

------------------------------------------------------------------------

A software bill of materials - **SBOM** (i.e. comprehensive list of the
third-party components and dependencies) must be added as an attachment.
This must include the following information:

-   Every third-party component (code, libraries, frameworks,
    dependencies - direct and transitive, images, data, fonts, etc.),
    the used version and the repository where it is made available;

-   Location in the tool and how it is integrated (code integration,
    static or dynamic linking);

-   The licence under which the third party-component is used;

-   Whether the developers have made any changes to the third-party
    component;

-   For the distribution of compiled code / object code, the compiler
    used.

The SBOM can be generated using a code scanning tool, a package manager
or a dependency management tool. However, EC services, particularly the
developers, must always double check the generated information and
complement it if there is a need to do so.

A correct (i.e. complete) identification of software in EURECA already
gives an indication of potential risks (just by looking at the types of
licences reported in the SBOM).

In addition to third-party code, EC services should identify the
inclusion of third-party text, images, data, fonts or other third-party
elements.

The SBOM will be used by the Central IP Service to perform the IP
clearance.

------------------------------------------------------------------------
**EURECA provides a Scope Checker
tool (link available on the EC internal FPIS ThinkOpen wiki)**[^7]
**that can help determine the cases where the software should be
identified.**
------------------------------------------------------------------------

#### STEP 2 - Intellectual property clearance (IP clearance)

IP clearance is a **mandatory step** before software distribution and is
carried out for all EC services by the Central IP Service.

To request IP clearance, email the IP helpdesk mailbox
<EC-IPR@ec.europa.eu>, after you have set up the EURECA file for the
software.

The conclusions of the IP clearance will be reflected in EURECA and
communicated to the requesting EC service by the Central IP Service.

The IP clearance aims to verify the ownership status of the software and
identify legal obstacles to the chosen distribution strategy. In
particular, the Central IP Service will examine:

-   for the parts developed by EC services or by external contractors
    for the Commission -- whether the EU has effective and sufficient
    ownership or co-ownership rights in the software

-   for the third-party components -- whether the software contains any
    elements which could hinder or affect the distribution of the
    software.

The Central IP Service will assess the software based on the information
the EC services entered into the EURECA system. A copy of the source
code (and, if the software is intended for distribution in an executable
format, the compiled version of the software) should also be sent to the
Central IP Service, either by giving access to the repository where the
code is available or by making it available for download.

Responsibility for providing a comprehensive list of third-party
components and dependencies remains with the EC service requesting the
IP clearance.

Initially, the IP clearance will include, without limitation, an
assessment of the following elements:

-   the list of third party-components (OS and/ or proprietary) and
    their licences;

-   the integration method;

-   the compilation method and the compiler, if the software is to be
    distributed in object code or executable format;

-   whether any modifications have been made to the components used;

Additional information will be taken into account for the purpose of the
IP clearance, depending on the particularities of the software under
analysis and the information provided in the SBOM.

In accordance with the Software Reuse Decision, software distributed by
the Commission should, in principle, be made available under an open
source licence. According to article 5 in the Software reuse Decision,
EUPL is the preferred choice.

However, other open source licences may be used, if, for instance:

-   this is required due to reciprocal clauses (*copyleft*) applicable
    to parts of the software originating from a third party;

-   a more permissive licence is deemed preferable, for instance to
    facilitate the adoption of the software by the community of users --
    for example, to encourage the inclusion of libraries in other
    projects.

Where, as a result of the licence clauses applicable to parts of the
software originating from a third party, a choice exists between several
standard open licences, not including the EUPL, preference shall be
given to the open source licence giving users the broader rights of use
(i.e. the most permissive licence). The choice of licence must be
determined together with the Central IP Service.

The licensing choice should then be reflected in the copyright notice
for all the files, together with a "LICENCE" file in the root folder
(for further details see Section IV.7 below).

The records in EURECA will be updated accordingly, to display the
conclusions of the IP clearance. The IP manager in charge of the dossier
will create a milestone in the timeline section of the software asset\'s
copyright file, identifying the date of the IP clearance.

------------------------------------------------------------------------

**EUPL**

The European Union Public Licence (EUPL) is an open source licence
designed by the European Commission and approved by OSI. The EUPL
enables the widest software dissemination while keeping the source code
open. The licence was specifically crafted to be fully compliant with EU
law, is available in 23 language versions and can be used by anyone for
software distribution.

It is a copyleft licence which means that distributed derivatives must
remain shared under the EUPL.

However, the EUPL provides compatibility options, so that when merged
with another work covered by GPL-2.0, GPL-3.0, LGPL, AGPL, CeCILL, OSL,
EPL or MPL, the combined work can be distributed under these licences.

The latest version of the licence -- the EUPL v.1.2 -- was published in
2017 in all EU languages (temporarily not in Irish[^8]) and can be found
at: <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.

The latest EUPL 1.2. Guidelines were published in 2021 and can be viewed
here:
<https://op.europa.eu/en/publication-detail/-/publication/c15c9e93-27e1-11ec-bd8e-01aa75ed71a1>.

------------------------------------------------------------------------

#### STEP 3 - Vulnerability assessment

The vulnerability assessment of the source code is a **mandatory step**
before software distribution and can be requested from the DIGIT.S1
Security Assurance sector, with its broad service offering of
Application Security Testing, Continuous Security Assurance (CSA)
service (link available on the EC internal FPIS ThinkOpen wiki),
which is integrated into CI/CD pipelines and the vulnerability
self-assessment service.

To request these services, use My IT
Support (MITS).
Alternatively, the assessment can be carried out directly by a
Commission department, if it has the dedicated expertise and resources
for performing this task. All such departments will be explicitly
identified, as far as is possible.

It should make no difference whether the vulnerability assessment is
carried out before, after or at the same time as the IP clearance, as
long as it is done before the software is made available in the EC
repository.

The outcome and recommendations resulting from the vulnerability
assessment on the source code will be communicated to the requesting EC
services, to enable them to carry on with the further steps in the
distribution flow.

Security verification should be an integral part of the software
development process, regardless of the dissemination policy.

The vulnerability assessment performed before software distribution
needs to ensure that the publication or sharing of EC source code does
not represent an actual or a potential risk to the security of the
information systems or databases of the Commission or of another
European institution, agency or body.

At the same time the vulnerability assessment aims to verify that the
software which is publicly distributed does not contain any secret or
sensitive information referring to personal data, systems, services or
databases which would otherwise endanger the EC and expose it to
security risks. Following IT security best practice and monitoring
security advisories, dependency checks should be carried out to make
sure that third-party open source components used are free from
vulnerabilities.

In a vulnerability assessment, all projects must meet the following
minimal requirements:

1.  **Secrets management** (the code cannot include digital
    authentication credentials, including passwords, keys, APIs,
    tokens);

2.  **Sensitive data management** (the code cannot include information
    on internal infrastructure, domains, internal IP addresses).

Additionally, depending on the intended distribution of the software,
the deployment on the EC systems and other factors, the vulnerability
assessment should also include a thorough review consisting of the
following:

1.  A verification through the EC Application Security Testing services
    before being made available to the official EC open code repository.
    It is required that all important vulnerabilities with severity high
    and medium discovered through application vulnerability assessment
    should be remedied before the code is published on the EC official
    repository. An example of the vulnerabilities classified by severity
    is presented in the figure below. For open source code, it could be
    defined as well that all low severity vulnerabilities need to be
    resolved, to further minimise the risk:


![](img/classification-vulnerabilities.png "image2")
<figcaption>
image 2
</figcaption>
1.  If source code is developed using the DevOps/DevSecOps engineering
    approach, the Continuous Security Assurance services should be used
    in the continuous integration/continuous development (CI/CD)
    pipelines, if technically feasible. The service implements automated
    security capabilities in the CI and CD pipelines, which are a
    prerequisite for enabling the development communities to access the
    service. The specific features of this service are described in the
    following Wiki:

    1.  Continuous Security Assurance (CSA) Service - EC DIGIT Security
        Assurance - EC Extranet Wiki (europa.eu).

2.  Code should be developed following the Commission standards and
    guidelines (link available on the EC internal FPIS ThinkOpen wiki)
    that are applicable and relevant for secure code development:

    1.  IT Security Standard - Web Application Security Standard.pdf
        (europa.eu);

    2.  Web Applications Secure Development Guidelines - EC DIGIT
        Security Assurance - EC Extranet Wiki (europa.eu).

EC services should undertake all necessary security testing, depending
on the nature or use of the software. If you are not sure which testing
you need to take, contact the Open Source Programme Office (OSPO) at
<eu-code-europa@ec.europa.eu>.

The contact point for discussing the most convenient approach for the
vulnerability assessment of the source code is the Security Assurance
team, in DIGIT.S1.

The records in EURECA will be updated correspondingly to display the
conclusions of the vulnerability assessment. The dossier owner will
create a milestone in the timeline section of the software asset\'s
copyright file, identifying the date the vulnerability assessment was
performed. The dossier owner is responsible for entering this
information accurately.

------------------------------------------------------------------------

After the IP clearance and the vulnerability assessment, the necessary
workflows are initiated to authorise the software asset\'s external
distribution or licensing, in line with the licensing policy and
Commission internal procedures:

1.  If the software will be made available under an **open source
    licence**, no further steps are involved and EC services can make
    the software available via the corresponding repository. The records
    in EURECA will be updated accordingly, to reflect the conclusions of
    the IP clearance and the vulnerability assessment.

ii\. If the software will be made available under the terms of a
**proprietary licence**, the delegation of powers in the field of
intellectual property set out in (SEC(2001) 1397 is applicable and a
Commission decision needs to be adopted (see point 4 below).

iii\. If the software will not be distributed under open source or
proprietary terms, but only made available as a **SaaS**, no further
steps are involved.

------------------------------------------------------------------------

### Procedure in case of proprietary distribution

In accordance with the [IP
delegation](link available on the EC internal FPIS ThinkOpen wiki)
and as confirmed by article 10 of the Software Reuse Decision, before
Commission software can be distributed under proprietary terms it is
necessary to adopt a Commission Decision allowing this distribution.

Following the 3-step procedure (the identification process in EURECA,
the IP clearance and the vulnerability assessment), the Commission
Decision workflow can be initiated.

The adoption of the Decision is under the responsibility of the Director
General of the DG in charge of the software project.

The Decision and the accompanying licence agreement must also pass
through an official inter-service consultation via Decide Consultation,
with the JRC, the Legal Service, the Secretariat-General and any other
concerned DGs.

------------------------------------------------------------------------

-   Before starting the Commission Decision adoption procedure, EC
    services should consult the Central IP Service. The Central IP
    Service will provide assistance in preparing the draft decision and
    the accompanying licence, tailored to the specific distribution
    strategy.

    You can contact the Central IP Service via the IP helpdesk mailbox
    <EC-IPR@ec.europa.eu>.

-   It is good practice to accompany the text of the Decision with a
    **note for the file** outlining in greater detail the background of
    the project and the software developed. This document will give
    senior management in the Service concerned the additional
    information they need to make an informed decision.

------------------------------------------------------------------------

The Commission Decision adoption procedure is carried out by a
legislative coordinator in the DG concerned and/or by a member of the
service thereto who has access rights in Decide as an author or as a
coordinator.

The list of **legislative coordinators** is available
 on the EC internal FPIS ThinkOpen wiki.

More details on the **adoption procedure** for the EC decision in the
guidelines (link available on the EC internal FPIS ThinkOpen wiki).

### Preparing the software for distribution

After identification in EURECA, the IP clearance, the vulnerability
assessment and, where applicable, after the Commission Decision has been
adopted, the EC service can proceed to distribute the software according
to the licensing terms agreed - open source or proprietary.

The services managing the repository where the code is being published
must ensure the requesting EC service has complied with the mandatory
steps for software distribution.

To distribute open source software, as per article 6 in the Software
Reuse Decision, the Commission must use a repository as a single point
of access to the software, to make it easy for users to access it and
reuse it.

_code.europe.eu -- the EC repository for OS projects_

The EU-centric digital code repository GitLab was launched in May 2022
and can be accessed at **<https://code.europa.eu>.**

Starting with its release and subject to technical capacity, this
repository will be the default one for all new and old EC software
released under open source terms.

Commission software shall be made available electronically in the
repository as human-readable source code and, when appropriate, in
machine-readable form and shall be accompanied by the necessary
documentation.

The obligation to make the software available in the EU-centric digital
code repository does not prevent EC services from using other
repositories -- where there are technologies that are strongly dependent
on specific platforms, or in order to reach certain communities.

## Software development process

### Planning ahead in the development lifecycle

Choices made when developing software have the potential to greatly
affect decisions on how it can be distributed. For this reason, it is
essential to consider the distribution strategy already during the
design phase of the development lifecycle.

Using open source software ("OSS") may be advantageous. However, since
legal risks exist, the use of OSS should be carefully monitored.
Developers are key people in this procedure.

The Central IP Service is available to provide further guidance,
additional clarification and support in specific cases. You can contact
them at <EC-IPR@ec.europa.eu>.

Additional information about open source software is available on the
Commission\'s IP website.

------------------------------------------------------------------------

**Key aspects**

The following aspects should be observed when developing software:

1.  Be aware of the software\'s intended distribution policy (open
    source distribution, proprietary distribution or no distribution);

2.  Document the development and, particularly, the inclusion of any
    third party OSS. Keep records \[Point 4\].

3.  If third-party OSS is included, comply with the licences under which
    it is distributed. If distribution of the software is envisaged, pay
    particular attention to incompatible licences \[Point 6\];

4.  Include as part of the distribution a copyright notice and the
    licence under which the software is distributed \[Point 8\];

5.  Include in the distribution all the copyright notices, disclaimers
    and licence agreements for the third party open source components
    \[Point 9\].

------------------------------------------------------------------------

Developers need to be aware of the distribution policy, even if the
decision is not taken by them. For instance, if it is decided that the
software will be subject to proprietary distribution, you cannot
incorporate code available under a \'copyleft\' licence (such as GPL).

The envisaged licence for the distribution should also be determined at
the earliest stage of the project, particularly when the aim is open
source distribution. In fact, the choice of a certain licence can
prevent the inclusion of certain open source components.

For development projects opting for open source distribution, EC
services should use an existing public open source licence. This early
choice is recommended in order to better select the open source
components that can be integrated into the development process. For
instance, if it is decided that the software must be distributed under
the EUPL open source licence, you cannot incorporate code available
under a copyleft licence (such as GPL).

EC services may seek support from the Central IP Service already at this
stage, if they have any doubt as to the choice of a suitable open source
licence.

-   As a rule of thumb, EC services should calibrate design choices to
    **account for a potential type of distribution, even where
    distribution is not planned** as a main objective of the project.
    This implies that, regardless of the distribution strategy, the code
    base should be adequately built and documented.

### Types of development projects

Development projects often begin with the gathering and assessment of
requirements that may be included in a business case. On the basis of
those business needs, before embarking on a brand new development EC
services should **verify whether a suitable code base already exists**
within available Commission catalogues (i.e. EURECA) or publicly
available projects.

Based on this assessment, EC services may opt to:

a.  **Start a project from a clean-slate code base**. This option confers

the widest discretion when choosing a distribution strategy, as the
project is not affected by the licensing limitations of any pre-existing
component or by the distribution strategy of existing projects.
Nevertheless later integration of third-party components in the
technology stack may anyway reduce or even determine the downstream
distribution options.

b.  **initiate a forked project**, i.e. one based on existing software.

Using an existing project as the basis for development may well be
justified by several considerations, including budget efficiency and
timeliness.

If the forked project is a Commission project, usually there is still a
considerable level of discretion in establishing a distribution
strategy. Where the project is instead forked from an external one, its
use is likely to require compliance with an already established
distribution strategy. For example using an existing open source project
as the basis for development may later require the compulsory
application of a specific licence for its distribution.

Depending on the case, this may be incompatible with the distribution
strategy initially chosen (e.g. proprietary distribution is not possible
if the development is based on a forked GPL project).

c.  A third possibility is that of **contributing code to an existing
    software project**.

According to the Software Reuse Decision, Commission services shall be
allowed to take part in and contribute to external open source projects
deemed to be in the interest of the EU or of one of its policy
objectives.

If required by the rules applying to the project, ownership of the
intellectual property rights on the contributed software may be
transferred to the public or private entity in charge of those open
source projects.

Contributing code to an existing software project is typically the case
with public open source projects where Commission code is contributed by
submitting pull requests (e.g. on GitHub). Existing public projects
usually already have a predetermined distribution strategy (e.g. open
source under EUPL).

The choice is limited to whether such distribution is acceptable for the
Commission depending on factors such as the benefits of the contribution
for the Commission or the resources that would need to be allocated in
order to contribute.

### Choosing suitable components

Modern software development projects make use of pre-existing components
such as open source libraries to speed up development and reduce the
costs associated with rewriting general-purpose code from scratch.

Since the distribution policy and the choice of the licence may have an
impact, for instance, on the possibility to include certain open source
components, all developers involved need to be aware of the distribution
policy. To this end, a design document containing the **distribution
strategy and its requirements should be shared with the developers\'
team**.

This also applies to external contractors assisting on a development
project. In case of procured development services, the design document
may be added to the service contract as an annex. Procurement officers
should ensure that the contractor is informed of the distribution policy
and that their contribution to the project complies with it [^9]

For instance, incorporating proprietary components in a software project
that pursues an open source distribution may expose the Commission to
copyright infringement allegations from the proprietors of those
components. On the other hand, adding components licensed under the open
source GPL licence will not permit downstream proprietary closed-source
distributions of the incorporating software.

Proprietary distributions may make use of libraries under permissive
open source licences such as the BSD and the MIT licences. However,
components under licences such as GPL, AGPL or EUPL licences should not
be integrated, as their viral effects would impede the application of
proprietary terms.

In general, when establishing the integration of open source components
in a Commission development project, it is always preferable to reduce
the number of OSS licences involved to a minimum. This is because the
higher the number of different OSS licences used, the higher the risk of
experiencing incompatibility issues. For example if an essential
component comes with a copyleft licence (e.g. GPL 3), it is advisable to
choose any additional components on the basis of either a permissive
open source licence (e.g. BSD or MIT) or based on the same copyleft
licence.

------------------------------------------------------------------------

The European Commission Open Source Programme Office (EC OSPO) was
created in 2020 as the first concrete action of the latest [Open Source
Software Strategy for
2020-23](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en).
It acts as a facilitator for activities outlined in the strategy and the
action plan guided by six principles: think open, transform, share,
contribute, secure, stay in control.

------------------------------------------------------------------------

### Keeping records

The use of OSS as part of a project needs to be carefully documented.
You should always record the following:

-   the origin of any OSS component used, its download date, its version
    number and the licence under which it is released;

-   how and where the OSS component is integrated into the larger
    programme;

-   the date, identification and description of modifications made to
    the OSS (if any).

------------------------------------------------------------------------

**CITnet**

It is recommended that all IT professionals and key business users in
the Commission who are involved in IT projects at the Commission to
develop EC software (including external contractors working on
Commission projects, both intra-muros or extra-muros) use the Commission
IT Network (CITnet) services.

CITnet is an information-sharing portal and a set of web collaborative
software development tools. CITnet and its web application lifecycle
management (ALM) toolset provide a collaborative approach inspired by
the open source communities\' way of working.

CITnet offers Version Control System (SVN, Git), Issue Tracker (Jira),
Agile Iteration Management, Collaborative Code Review
(Fisheye/Crucible), Software Code Repository (Nexus), Continuous
Integration (Bamboo) and Collaborative Documentation
(Confluence/Gliffy/Balsamiq).

Details about CITnet available on the EC internal FPIS ThinkOpen wiki.

CITnet provides developers and, when needed, IP correspondents with all
the relevant information about the development of software and the build
process involved. On CITnet, the actual code and its revisions are
managed. It keeps track of code \"commits\", the committers and commit
dates.

The developers must also adequately document how to use the code -- how
to run it, or how to build a binary application. As the aim is to make
the software available, the documentation must list all of the
requirements and dependencies on other software that have to be
available on the system to run, interpret or compile the code. Also, the
documentation must indicate what other tools and solutions need to be
available.

Documenting the development will help generate the necessary legal
conditions for the relevant Commission service to arrange for its
distribution at a later point in time.

------------------------------------------------------------------------

### Suitable clauses in the procurement contracts

Custom software development may be either procured, performed by
Commission services or combine both. When procurement is involved,
procurement staff should ensure upstream, **via the appropriate clauses
in the contracts**, that the Commission obtains from contractors the
necessary information about the licences to be able to further develop
and distribute the resulting code, if it plans to do so.

For procurement contract purposes, if the software commissioned will be
distributed, it is recommended that EC services already consider the
licensing policy and reflect it in the procurement documentation, so
that the contractor ensures that the choice of pre-existing software
components, if any, is compatible with the chosen licensing policy.

-   If the Commission intends to release the results under a standard
    open source licence, it is relevant to distinguish between the types
    of available licences, namely permissive or copyleft licences. EC
    services should start by considering the EUPL, which, according to
    the Software Reuse Decision, is the preferred choice for
    distributing software for which the EU holds the IP rights. However,
    they should also take into account the nature of the software, as
    some licences might be more suitable than others for distributing
    libraries, plugin modules or other functional applications (designed
    to provide out-of-the-box functionalities to other programs).

    There might also be cases when the choice of licensing regime would
    be restricted from the start by different factors, such as the
    technology the software is supposed to rely on or the nature of the
    commissioned software.

-   if the Commission intends to release the results under a proprietary
    licence or under a permissive open source licence, the contractual
    clauses should prevent the contractor using pre-existing software
    under a copyleft regime (at least whenever such use would trigger
    the copyleft effect of the licence).

EC services involved in procurement are invited to contact the Central
IP Service when choosing the appropriate provisions for procurement
contracts.

### Limitations of open source licences: conditions and licence compatibility

The use of OSS is only legal as long as the user stays within the limits
of the permitted uses and respects the conditions imposed by the
respective OSS licences.

The conditions of use and the obligations imposed vary according to the
licence. Examples include the identification of the creator(s), the
documentation of changes, the distribution of the source codes or the
distribution of the software and any changes to it under the same or
under a compatible licence.

\*Note: combining software components accompanied by incompatible
licences may lead to copyright infringement if the resulting software is
distributed.\*

Open Source Licence A is said "compatible" with an Open Source Licence B
when software originally licenced under A may be put together or
re-distributed under licence B after modification or merger with other
software. Compatibility is not necessary reciprocal: it is possible that
licence A is compatible with licence B, while licence B is not
compatible with licence A.

Some OSS licences (e.g. the different versions of GPL, AGPL and EUPL)
make it mandatory to distribute the software and any modifications to
under the same or a compatible licence (copyleft). For instance, if
software released under GPLv3 is modified or integrated as part of a
larger work, the distribution of the new work is only possible under
GPLv3. In general, two different copyleft licences are unavoidably
incompatible **unless they include explicit compatibility provisions
(this is why GPLv2 is incompatible with GPLv3).**

Some examples of licence compatibility are provided below:

-   Example 1: Merger between two software components, one released
    under the 3-Clause BSD License and the other under the EUPL 1.2

    The software originally released under the 3-Clause BSD Licence can
    be integrated into a final result which is distributed under EUPL
    1.2 after being merged with other software. The terms of the
    3-Clause BSD Licence authorise this subsequent change of licence.
    Hence, the 3-Clause BSD Licence is compatible with EUPL 1.2.

    However, the distribution of the same result under the 3-Clause BSD
    Licence is not possible, because the terms of the EUPL 1.2 require
    that derivative works of software released under the EUPL 1.2 are
    also released under the EUPL 1.2 (copyleft). We could say that EUPL
    1.2 is not compatible with the 3-Clause BSD Licence.

This is thus a case where compatibility between BSD and EUPL is not
reciprocal.

-   Example 2: Linking between two software components, one released
    under EUPL 1.2 and the other under the GPLv3

    The result cannot be distributed under the EUPL 1.2, as the GPLv3
    requires that the result is released under the GPLv3. This operation
    is thus impossible without infringing the terms of the GPLv3. The
    GPLv3 is not compatible with the EUPL.

    Although the EUPL 1.2 also requires that derivative works are
    normally released under the EUPL, it contains a clause which
    explicitly allows derivative works to be released under the GPLv3 in
    the event of a merger between EUPL and GPLv3 software.

    This is a case of express compatibility: the EUPL 1.2 is compatible
    with the GPLv3. Here again, we could note that compatibility between
    EUPL 1.2 and GPLv3 is not reciprocal.

-   **A list of licences compatible with GPLv2 and GPLv3** is available
    at <https://www.gnu.org/licenses/license-list.html>.

-   **A matrix of EUPL compatible open source licences** is available at
    <https://joinup.ec.europa.eu/collection/eupl/matrix-eupl-compatible-open-source-licences>.
    However, please note that the Central IP Service does not
    necessarily completely share the views provided as regards dynamic
    linking with GPL, AGPL and similar licences.

------------------------------------------------------------------------

As a rule, the copyleft effect is only triggered if the OSS is
distributed externally. This would mean that developing software with
incompatible licences would not prevent its internal use within the
Commission. However, this should not be done unless it is clearly
decided that the software will never be distributed outside the
Commission.

------------------------------------------------------------------------

### Distribution of object code and the use of compilers

A compiler is routinely used in software development to translate source
code to a lower level programming language (e.g. binary or object code),
in order to create an executable program. As part of their functioning,
compilers may inject elements into the resulting code or embed libraries
within the resulting executable file.

Different compilers may have different terms and conditions associated
with the redistribution of the elements they introduce during the
compilation process, thereby sometimes influencing the legal
considerations made in choosing a licence for the release of the
executable program.

In essence, compilers do not have a tendency to prevent the release or
distribution of the executable program. However, certain proprietary
compilers (for example, some Intel software development products, among
others) do require that the resulting executable software is distributed
subject to a licence agreement that prohibits disassembly and reverse
engineering.

This means that an executable program compiled using, for example, the
Intel Fortran Compiler, may need to be released under an end-user
licence agreement which contains a clause that prohibits disassembly and
reverse engineering.

As these legal considerations have to be taken into account before
releasing the software, it is important that the Central IP Service is
informed of the exact compiler used to create the executable version.

Additionally, particular attention should be paid to the different
provisions of licences (whether open source or proprietary) concerning
the use of binaries (for example some proprietary licences may allow the
distribution of binaries, but prohibit the distribution of the source
code, while some open source licences such as the EUPL or GPL require
that whenever the executable code of a program is distributed, a copy of
the source code is provided alongside).

### Including the EU copyright notice and the licence under which the software is distributed

All the software distributed by the Commission should include
information on its copyright ownership and, where applicable, on its
distribution terms.

The standard method of doing this is to include the copyright notice and
the licence text in a file named \"LICENCE\".

In addition, a short copyright notice should be included at the top of
each source file. Examples of these are provided below:

**For proprietary distribution:**

> \"/\*
>
> Copyright (c) 2021 European Union. All rights reserved./
>
> \*/.

**For distribution under EUPL 1.2:**

> \"/\*
>
> Copyright (c) 2021 European Union/
>
> -   
>
> Licensed under the EUPL, Version 1.2 or -- as soon they will be
> approved by the European Commission -- subsequent versions of the EUPL
> (the \"Licence\");
>
> You may not use this work except in compliance with the Licence.
>
> You may obtain a copy of the Licence at:
>
> -   
>
> [<https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12/>](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)
>
> -   
>
> Unless required by applicable law or agreed to in writing, software
> distributed under the Licence is distributed on an \"AS IS\" basis,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
> implied. See the Licence for the specific language governing
> permissions and limitations under the Licence.
>
> \*/.

### Third-party notices and disclaimer

OSS usually contains copyright notices and disclaimers in its source
code or in attached files. These typically indicate the name and version
number of the software, the identity of the creator (or of the different
contributors), the reference (or the full text) of the licence under
which it is released and a disclaimer or a statement indicating where
the source code is available and information on how to obtain it. One
basic compliance measure when using open source code is to keep
(unaltered) these original copyright notices and disclaimers included
with each component\'s package and file header.

A .txt file (ideally named "Notice") should be created in the root
folder. The file must list the third-party components used along with
the copyright notices and the text of the licences.

For short licences (e.g. MIT, BSD), the suggestion is to include the
entire licence along with the copyright notice. Example:

> -   Ecdsa
>
>     [<https:github.comtlsfuzzerpython-ecdsa>](https://github.com/tlsfuzzer/python-ecdsa)
>
> MIT Licence
>
> \"python-ecdsa\" Copyright (c) 2010 Brian Warner
>
> Portions written in 2005 by Peter Pearson and placed in the public
> domain.
>
> Permission is hereby granted, free of charge, to any person obtaining
> a copy of this software and associated documentation files (the
> \"Software\"), to deal in the Software without restriction, including
> without limitation the rights to use, copy, modify, merge, publish,
> distribute, sublicense, andor sell copies of the Software, and to
> permit persons to whom the Software is furnished to do so, subject to
> the following conditions:
>
> The above copyright notice and this permission notice shall be
> included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
> MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
> IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
> CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
> TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
> SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For more complex licences (Apache, GPL), it is enough to include only a
short reference along with the copyright notice, to avoid repeating the
texts of the licences several times.

Example for GPL v3:

> ggeo.c
>
> <https:github.comec-jrcjeolib-miallib>
>
> GPL v.3 Licence
>
> Copyright (c) 2020-2022 European Union
>
> This program is free software: you can redistribute it andor modify it
> under the terms of the GNU General Public License as published by the
> Free Software Foundation, either version 3 of the License, or (at your
> option) any later version.
>
> This program is distributed in the hope that it will be useful, but
> WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
> General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program. If not, see <https:www.gnu.orglicenses>.

The entire text of the longer licences should be provided in the text
file after you have listed all the components with their attributions:

> The terms of the Apache 2.0 Licence (covering tensorflow) are herewith
> provided:
>
> Apache License
>
> Version 2.0, January 2004
>
> [<http:www.apache.orglicenses>](http://www.apache.org/licenses/)
>
> \[text of licence\]
>
> END OF TERMS AND CONDITIONS

## Resources

### Information and materials on intellectual property and open source[^10]
(links available on the EC internal FPIS ThinkOpen wiki)

- EURECA manual and scope checker;
- IPR risk management guidelines;
- Explanatory note on clauses regarding intellectual property rights in model service contracts used by the Commission;
- List of free open source code scanning tools for Java, JavaScript and PHP (as identified by the DevSecOps team in unit DIGIT.A.4;
- Available e-learning courses and training on the EU Learn platform.

### Security information[^11]
(links available on the EC internal FPIS ThinkOpen wiki)

-   Security Assurance Wiki: Services - EC DIGIT Security Assurance -
    EC Extranet Wiki
 

-   Security Assurance service offering entries in DIGIT Service
    Catalogue:

    -   Application Security Testing \| DIGIT Service Catalogue
       

    -   Continuous Security Assurance services \| DIGIT Service
        Catalogue
    

    -   Vulnerability Self-Assessment (VSA) - Static Application Security Testing
        (SAST) Procedure - EC DIGIT Security Assurance

-   EC IT Security Standards:

    -   IT Security Policy, Standards, Guidelines and Technical
        specifications
        

### Joinup

Joinup is a collaborative platform created by the European Commission
and funded by the EU via the ISA2 programme (interoperability solutions
for public administrations, businesses and citizens). It offers several
services that aim to help e-Government professionals share their
experience with each other, and help them find, choose, re-use, develop
and implement interoperability solutions -- see
<https://joinup.ec.europa.eu/>.

The platform offers information and several tools that are useful for
understanding and choosing the most appropriate OS licence:

-   **Licensing Assistant**: an interactive web-tool to help you choose
    the most appropriate open source licence:
    <https://joinup.ec.europa.eu/collection/eupl/joinup-licensing-assistant-jla>;

-   **Compatibility checker**:
    <https://joinup.ec.europa.eu/collection/eupl/solution/joinup-licensing-assistant/jla-compatibility-checker>

### EUPL

-   [All official versions of the European Union Public Licence
    (EUPL-1.2)](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12);

-   Comprehensive information on the EUPL (such as guidelines, notes
    about licence compatibility, a matrix of EUPL compatible licences,
    EUPL FAQs and a documentation directory), available on the
    [dedicated space on the Joinup
    platform](https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics);

-   EUPL 1.2.
    Guidelines (link available on the EC internal FPIS ThinkOpen wiki)

### Contact points

-   Central Intellectual Property (IP) Service: <EC-IPR@ec.europa.eu>

-   Open Source Programme Office:
    EU-CODE-EUROPA@ec.europa.eu

-   EC DIGIT Security Assurance Sector:
    <EC-DIGIT-SECURITY-ASSURANCE@ec.europa.eu>

## Footnotes

[^1]: When reference is made to EC services, these are to be understood
    as departments of the Commission (such as a unit, for instance).

[^2]: Communication to the Commission, Open Source Software Strategy
    2020-2023, Think Open, 21.10.2020, C(2020) 7149 final.

[^3]: Commission Decision of 8 December 2021 on the open source
    licensing and reuse of Commission software 2021/C 495 I/01 (OJ C,
    C/495, 09.12.2021.

[^4]: Including bug fix updates.

[^5]: The decision shall be taken by the EC services responsible for the
    software, on a case-by-case basis, depending on factors such as the
    scope of the contribution (e.g. how many human resources it
    requires, how much time it will take, etc.). The more significant
    the impact of the contribution is for the EC services, the higher
    should be the level of the management making the decision (in line
    with the normal rules applying in the DG concerned for other
    decisions in other fields).

[^6]: ITCB
    Mandate (link available on the EC internal FPIS ThinkOpen wiki)
    ITIT
    Mandate (link available on the EC internal FPIS ThinkOpen wiki)

[^7]: The Scope Checker is accessible only to EC services.

[^8]: The Irish version should be available in 2023.

[^9]: For instance DG DIGIT has developed its own contract templates on
    open source procurement.

[^10]: The links provided under this section are accessible only to EC
    services.

[^11]: The links provided under this section are accessible only to EC
    services.
